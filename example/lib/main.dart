import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_widegt_wangyan/flutter_widegt_wangyan.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("测试页面"),
      ),
      backgroundColor: Colors.white,
      body: SettingsPage(),
    );
  }

  WyCalenderController _controller = WyCalenderController();
  @override
  void initState() {
    super.initState();
  }
}

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

// 1 实现 SingleTickerProviderStateMixin
class _SettingsPageState extends State<SettingsPage>
    with TickerProviderStateMixin {
  List<WyCascaderData> value;

  List<WyCascaderData> options = [
    WyCascaderData(
        text: '浙江省',
        value: '330000',
        children: [WyCascaderData(text: '杭州市', value: '330100', children: [])]),
    WyCascaderData(text: '江苏省', value: '320000', children: [
      WyCascaderData(text: '南京市', value: '320100', children: [
        WyCascaderData(text: "武隆区", value: "320110"),
        WyCascaderData(text: "大奖区", value: "320120"),
        WyCascaderData(text: "龙社区", value: "320130"),
        WyCascaderData(text: "武隆区", value: "320110"),
      ])
    ]),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: WyButton(
          text: 'WyCascader',
          onClick: () {
            WyCascader.show(context,
                value: value, activeColor: Colors.blue, options: options,
                fisinsh: (val, selectedOptions, tabIndex, callback) {
              value = selectedOptions;
            }, change: (val, selectedOptions, tabIndex, callback) {
              if (val == '330100') {
                Timer(Duration(seconds: 3), () {
                  options[0].children[0].children = [
                    WyCascaderData(text: "西湖区", value: "310120"),
                    WyCascaderData(text: "萧山区", value: "310130"),
                    WyCascaderData(text: "滨江区", value: "310110"),
                  ];
                  callback();
                });
              }
            });
          },
        ),
      ),
    );
  }
}
