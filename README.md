# flutter_widegt_wangyan

一个flutter的ui库，其中包含多种常用的组件

##  WyButton 按钮

按钮用于触发一个操作，如提交表单

**代码示例**

```dart
WyButton(
	text: '大按钮',
	type: ButtonType.info,
	size: ButtonSize.large,
	onClick: (){},
),
WyButton(
	text: '迷你按钮',
	type: ButtonType.warning,
	block: false,
	size: ButtonSize.mini,
	onClick: (){},
),
WyButton(
	text: '常规按钮',
	type: ButtonType.danger,
	block: false,
	size: ButtonSize.normal,
	onClick: (){},
),
WyButton(
	text: '小按钮',
	type: ButtonType.primary,
	size: ButtonSize.small,
	block: false,
	onClick: (){},
),
```
**参数说明**

|参数 |   说明  | 类型  |  默认值 |
| :------------ | :------------ | :------------ | :------------ |
|  text | 按钮文字  | Sting  |  按钮 |
|  type | 类型，可选值为 primary info warning danger  | ButtonType  |  ButtonType.primary |
|  plain | 是否为朴素按钮  | bool  |  false |
|  hairline | 是否使用 0.5px 边框  | bool  |  false |
|  loading | 是否显示为加载状态  | bool  |  false |
|  block | 是否为块级元素  | bool  |  false |
|  shape | 按钮的形状，可选择为square，round  | ShapeType  |  ShapeType.square |
|  loadingType | 加载器的类型  | ShapeType  |  ShapeType.square |
|  size | 尺寸，可选值为 large small mini，normal  | ButtonSize  |  ButtonSize.normal |
|  disabled | 是否禁用按钮  | bool  |  false |
|  icon | 左侧图标名称  | IconData  |  null |
|  color | 自定义主题颜色  | Color  |  null |
|  onClick | 点击事件  | VoidCallback  |  null |
|  style | 按钮的主题，详情请看下表  | WyButtonStyle  |  null |

**WyButtonStyle**

参数color的优先级要高于WyButtonStyle，设置color可以覆盖WyButtonStyle中的backgroundColor,borderColor

|参数 |   说明  | 类型  |  默认值 |
| :------------ | :------------ | :------------ | :------------ |
| backgroundColor  |  按钮的背景颜色 |  Color | Colors.white  |
| borderColor  | 按钮的边框颜色，当plain为true时，有效  |  Color | Color(0xffeaebec)  |
| textColor  | 按钮的文字颜色  |  Color | Colors.black  |
| miniPadding  | 但按钮的type为mini时，按钮内的默认padding  |  EdgeInsets | EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0)  |
| smallPadding  | 但按钮的type为small时，按钮内的默认padding  |  EdgeInsets | EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0)  |
| normalPadding  | 但按钮的type为normal时，按钮内的默认padding  |  EdgeInsets | EdgeInsets.symmetric(horizontal: 18.0, vertical: 12.0)  |
| largePadding  | 但按钮的type为large时，按钮内的默认padding  |  EdgeInsets | EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0)  |

## WyPopup 弹出层

弹出层容器，用于展示弹窗、信息提示等内容

**代码示例**
```dart
	WyPopup.showSheet<num>(
	    context,
		height: 300,
		builder: (context) => Container(
			alignment: Alignment.center,
			 child: FlatButton(
				 child: Text('按钮'),
				 onPressed: () => {Navigator.pop(context, 100)},
			 ),
		),
	)).then((onValue) => {print(onValue)})
```
**参数说明**

|参数 |   说明  | 类型  |  默认值 |
| :------------ | :------------ | :------------ | :------------ |
| overlay  |  是否显示遮罩层 | bool  | true  |
| overlayColor  | 弹出层空白区域的颜色如果为null，则为透明色  |  Color |  null |
| position  | 弹出层的位置，可选值为top, bottom, right, left |  PopupPosition |  PopupPosition.bottom |
| closeOnClickOverlay  |  是否在点击遮罩层后关闭 | bool  | true  |
| closeable  |  是否显示关闭图标 | bool  | false  |
| closeIcon  |  关闭按钮的widget | IconData  | icons.cancel  |
| iconPosition  |  关闭按钮的位置 | PopupCloseIconPosition  | PopupCloseIconPosition.topRight  |
| closeIconBuilder  |  关闭按钮的构造方法，优先级高于closeIcon | WidgetBuilder  | null  |
| curve  |  动画类名, 动画的运动曲线 | Curve  | Curves.ease  |
| height  |  弹出层的高度 当弹出层的位置position为left或者right时，该值无效 | double  | null  |
| width  |  弹出层的宽带 | double  | null  |
| round  |  是否显示圆角 | bool  | false  |
| builder  |  弹出层的内容区域 | WidgetBuilder  | null  |
| style  |  弹出层的主题，详情请看下表 | WyPopupStyle  | WyPopupStyle.defaultTheme  |

**WyPopupStyle**

|参数 |   说明  | 类型  |  默认值 |
| :------------ | :------------ | :------------ | :------------ |
| primaryColor  |  弹框内容的背景色 |  Color |  Colors.white |
|  iconSize |  closeIcon的大小 | double  |  22 |
|  iconColor |  closeIcon的颜色 | Color  |  Colors.white |
|  width |  当position为left或right时，弹框的默认宽度 | double  |  260 |
|  height |  当position为top或bottom时，弹框的默认高度 | double  |  360 |
## WyToast 轻提示

在页面中间弹出黑色半透明提示，用于消息通知、加载提示、操作结果提示等场景。

**代码示例**

   ```dart
WyToast(context, message: "提示文字");
onClick: () async {
	WyToast.loading(context, message: "登录中", duration: 0);
	await Future.delayed(Duration(seconds: 2));
	WyToast.remove();
}
```
**方法**

|参数 |   说明  | 参数  |  返回 |
| ------------ | ------------ | ------------ | ------------ |
|  WyToast |  展示提示 | options  |  WyToast 实例 |
|  WyToast.loading |  展示加载提示 | options  |  WyToast 实例 |
|  WyToast.fail |  展示失败提示 | options  |  WyToast 实例 |
|  WyToast.success |  展示成功提示 | options  |  WyToast 实例 |
|  WyToast.remove |  关闭提示 | -  |  void |

**参数说明(options)**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  message |  文本内容, 必填项 | String  |  null |
|  context |  上下文环境，必填项 | BuildContext  |  null |
|  forbidClick |  是否禁止背景点击 |  bool | false  |
|  overlay |  是否显示背景遮罩层 |  bool | false  |
|  duration |  展示时长(ms)，值为 0 时，toast 不会消失 |  double | 2000  |
|  animationTime |  toast动画的animation时长 |  Duration | Duration(milliseconds: 200) |
|  position |  位置，可选值为 top bottom center | Position  | Position.center  |
|  iconData |  自定义图标 |  IconData | null  |
|  iconBuilder |  自定义icon的构造函数 |  WidgetBuilder | null  |
|  type |  提示类型，可选值为 message loading success fail widget | ToastType  | ToastType.message  |
|  style | WyToast的默认样式,详情看下表  |  WyToastStyle | WyToastStyle.defaultStyle  |

**wyToastStyle**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  toastMaxWidth |  WyToast最大宽度 | String  |  70% |
|  toastFontSize |  WyToast的字体大小 | double  |  16 |
|  toastTextColor |  WyToast的字体的颜色 | Color  |  Colors.white |
|  toastLoadingIconColor |  WyToast的加载器的颜色 | Color  |  Colors.white |
|  toastLineHeight |  WyToast的字体行高 | double  |  20 |
|  toastBorderRadius |  WyToast的圆角大小 | Radius  |  Radius.circular(8.0)|
|  toastBackgroundColor |  WyToast的背景颜色 | Color |  Color.fromRGBO(0, 0, 0, 0.7)|
|  toastOverlayBackgroundColor |  WyToast的背景蒙层的颜色 | Color |  Color.fromRGBO(0, 0, 0, 0.3)|
|  toastIconSize |  WyToast的icon图标大小 | double  |  60 |
|  toastTextPadding |  WyToast的type类型为ToastType.message时的padding值 | EdgeInsets  |  EdgeInsets.symmetric(horizontal: 10, vertical: 12) |
|  toastDefaultPadding |  WyToast的type类型不为ToastType.message时的padding值 | double  |  30 |
|  toastDefaultWidth |  WyToast的type类型不为ToastType.message时的width | double  |  150 |
|  toastPositionTopDistance |  WyToast距离top的值 | String  |  20% |
|  toastPositionBottomDistance |  WyToast距离bottom的值 | String  |  20% |

## WyImage 轻提示

增强版的 img 标签，提供多种图片填充模式，支持图片懒加载、加载中提示、加载失败提示。

**代码示例**

```dart
Container(
	alignment: Alignment.center,
	child: Row(
		mainAxisAlignment: MainAxisAlignment.start,
		crossAxisAlignment: CrossAxisAlignment.start,
		children: srcList.map((item) {
			return WyImage(
				src: item,
				width: 150,
				height: 150,
				fit: BoxFit.cover,
				lazyLoad: true,
				radius: Radius.circular(20.0),
			);
		}).toList(),
	))
```
**参数说明**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  src |  图片链接,必填项 | String  |  null |
|  width |  宽度，必填项 | double  |  null |
|  height |  高度， 必填项 | double  |  null |
|  fit |  图片填充模式 | BoxFit  |  BoxFit.fill |
|  radius |  圆角大小 | Radius  |  Radius.circular(0.0) |
|  round |  是否显示为圆形或椭圆形，如果width和height相同则为圆形否则为椭圆 | Radius  |  Radius.circular(0.0) |
|  lazyLoad |  是否开启图片懒加载，须配合 WyLazyLoadScrollView 组件使用 | bool  |  false |
|  showError |  是否展示图片加载失败提示 | bool  |  true |
|  showLoading |  是否展示图片加载中提示 | bool  |  true |
|  errorIcon |  失败时提示的icon图标 | IconData  |  Icons.broken_image |
|  loadingIcon |  加载时提示的icon图标 | IconData  |  Icons.image |
|  loadingBuilder |  加载时提示widget的构造函数 | WidgetBuilder  |  null |
|  errorBuilder |  失败时提示widget的构造函数 | WidgetBuilder  |  null |
|  errorBuilder |  失败时提示widget的构造函数 | WidgetBuilder  |  null |
|  click |  点击图片时触发 | Function  |  null |
|  load |  图片加载完毕时触发 | Function  |  null |
|  error |  图片加载失败时触发 | Function  |  null |
|  progress |  图片加载过程中时触发 | ImageChunkListener  |  null |
|  style |  组件的样式，详情请看下表 | WyImageStyle  |  WyImageStyle.defaultStyle |

**WyImageStyle**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  imagePlaceholderBackgroundColor |  WyImage的默认背景色 | Color  |  Color.fromRGBO(247, 248, 250, 1) |
|  imageLoadingIconSize |  WyImage加载状态的默认Icon大小 | double  |  48 |
|  imageLoadingIconColor |  WyImage加载状态的默认Icon颜色 | Color  |  Color.fromRGBO(220, 222, 224, 1) |
|  imageErrorIconSize |  WyImage错误状态的默认Icon大小 | double  |  48 |
|  imageErrorIconColor |  WyImage错误状态的默认Icon颜色 | Color  |  Color.fromRGBO(220, 222, 224, 1) |


## WyLazyLoadScrollView

图片懒加载，需要WyImage开启图片懒加载模式。

**代码示例**

```dart
Container(
	child: WyLazyLoadScrollView(
		child: Container(
			alignment: Alignment.center,
			child: Column(
				mainAxisAlignment: MainAxisAlignment.start,
				crossAxisAlignment: CrossAxisAlignment.start,
				children: srcList.map((item) {
					return WyImage(
						src: item,
						width: 150,
						height: 150,
						fit: BoxFit.cover,
						lazyLoad: true,
						radius: Radius.circular(20.0),
					);
				}).toList(),
			)),
	)
)
```

**参数说明**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  scrollParams |  ScrollView的参数 | ScrollParams  |  const ScrollParams() |
|  child |  WyLazyLoadScrollView的子组件，必填项 | Widget  |  null |
|  loadingWidget |  加载时的图片 | Widget  |  null |
|  errorWidget |  错误时的图片 | Widget  |  null |
|  preload |  预加载高度 | double  |  20 |
|  attempt |  尝试次数 | int  |  3 |
|  filter |  图片 URL 过滤 | FilterFunction  |  null |

## WyCalendar 日历

日历组件用于选择日期或日期区间

**代码示例**

```dart
WyButton(
	text: "日历按钮",
	type: ButtonType.primary,
	onClick: () async {
		WyCalendar.show(
			context,
			maxRange: 3,
			position: PopupPosition.bottom,
			type: CalenderType.range,
			color: Colors.blue,
			formatter: (Date day) {
				int month = day.date.month,
					date = day.date.day;
				if (month == 6) {
					if (date == 1) {
						day.topInfo = '儿童节';
					} else if (date == 14) {
						day.topInfo = '端午节';
					} else if (date == 1 || date == 2) {
						day.type = DayType.disabled;
					}
				}

				if (month == 7) {
					if (date % 2 == 0) {
						day.type = DayType.disabled;
					}
				}
				return day;
			}).then((value) {
			print(value);
		});
	},
),
```

**参数说明**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  type |  选择类型:single表示选择单个日期，multiple表示选择多个日期，range表示选择日期区间 | CalenderType  |  CalenderType.single |
|  title |  日历标题 | String  |  日历 |
|  color |  主题色，对底部按钮和选中日期生效 | Color  |  Colors.red |
|  minDate |  可选择的最小日期 | DateTime  |  当前日期 |
|  maxDate |  可选择的最大日期 | DateTime  |  当前日期的12个月后 |
|  defaultDate |  默认选中的日期，type 为 multiple 或 range 时为数组，传入 null 表示默认不选择 | List<DateTime>  |  null |
|  rowHeight |  日期行高 | double  |  64 |
|  formatter |  日期格式化函数 | CalendarFormatter  |  null |
|  showMark |  是否显示月份背景水印 | bool  |  true |
|  showTitle  |  是否展示日历标题 | bool  |  true |
|  showSubtitle  |  是否展示日历副标题（年月） | bool  |  true |
|  showConfirm  |  是否展示确认按钮 | bool  |  true |
|  readonly  |  是否为只读状态，只读状态下不能选择日期 | bool  |  false |
|  confirmText  |  确认按钮的文字 | String  |  确定 |
|  confirmDisabledText  |  确认按钮处于禁用状态时的文字 | String  |  确定 |
|  titleBuilder  |  自定义标题 | WidgetBuilder  |  null |
|  footerBuilder  |  自定义底部区域内容 | WidgetBuilder  |  null |
|  controller  |  控制日历组件的方法详情看下表 | WyCalenderController  |  null |
|  style  |  控制日历组件的默认样式详情看下表 | WyCalendarStyle  |  null |
|  select  |  点击并选中任意日期时触发 |  CalendarSelect | null |
|  monthShow  |  当某个月份进入可视区域时触发 |  CalendarMonthShow | null |

**WyCalenderController**

|  方法名 |  说明 | 参数  |  返回值 |
| ------------ | ------------ | ------------ | ------------ |
|  reset |  将选中的日期重置到指定日期，未传参时会重置到默认日期 | date?: DateTime[]  |  - |
|  scrollToDate |  滚动到某个日期 | date: DateTime  |  - |

**WyCalender.show(context, props)**

当 WyCalendar 使用show方法调用时，支持以下 props。

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  position  |  弹出位置，可选值为 top right left |  PopupPosition | PopupPosition.bottom  |
|  round  |  是否显示圆角弹窗 |  bool | true  |
|  closeOnClickOverlay  |  是否在点击遮罩层后关闭 |  bool | true  |


**Range Props**
当 WyCalendar 的 type 为 CalenderType.range 时，支持以下 props:

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  maxRange  |  日期区间最多可选天数 |  int | 无限制  |
|  rangePrompt  |  范围选择超过最多可选天数时的提示文案，其中 xx 为最大天数占位符 |  String | 选择天数不能超过 xx 天  |

**Multiple Props**
当 Calendar 的 type 为 multiple 时，支持以下 props:

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|  maxRange  |  日期区间最多可选天数 |  int | 无限制  |
|  rangePrompt  |  范围选择超过最多可选天数时的提示文案，其中 xx 为最大天数占位符 |  String | 选择天数不能超过 xx 天  |

**Day 数据结构**
日历中的每个日期都对应一个 Day 对象，通过formatter属性可以自定义 Day 对象的内容

|键名 |   说明  | 类型  |
| ------------ | ------------ | ------------ |
|  date  |  日期对应的 Date 对象 |  DateTime |
|  type  |  日期类型，可选值为selected、start、middle、end、disabled |  DayType |
|  text  |  中间显示的文字 |  String |
|  topInfo  |  中间显示的文字 |  String |
|  bottomInfo  |  中间显示的文字 |  String |

**WyCalendarStyle**

| 名称 |   默认值  | 描述  |
| ------------ | ------------ | ------------ |
| backgroundColor |   Colors.white  | -  |
| popupHeight |   80%  | -  |
| headerTitleHeight |   44  | -  |
| headerTitleFontSize |   16  | -  |
| headerSubtitleFontSize |   14  | -  |
| weekdaysHeight |   30  | -  |
| weekdaysFontSize |   12  | -  |
| monthTitleFontSize |   14  | -  |
| monthMarkColor |   Color.fromRGBO(242, 243, 245, 0.8)  | -  |
| monthMarkFontSize |   160  | -  |
| dayHeight |   64  | -  |
| dayFontSize |   16  | -  |
| rangeEdgeColor |   Colors.white  | -  |
| headerBoxShadow |  [ BoxShadow(color: Color.fromRGBO(125, 126, 128, 0.16),offset: Offset(0.2, 10),blurRadius: 10,spreadRadius: 0)] | -  |

## WyCascader 级联选择
级联选择框，用于多层级数据的选择，典型场景为省市区选择

**代码示例**

```dart
class _SettingsPageState extends State<SettingsPage>
    with TickerProviderStateMixin {
  List<WyCascaderData> value;

  List<WyCascaderData> options = [
    WyCascaderData(
        text: '浙江省',
        value: '330000',
        children: [WyCascaderData(text: '杭州市', value: '330100', children: [])]),
    WyCascaderData(text: '江苏省', value: '320000', children: [
      WyCascaderData(text: '南京市', value: '320100', children: [
        WyCascaderData(text: "武隆区", value: "320110"),
        WyCascaderData(text: "大奖区", value: "320120"),
        WyCascaderData(text: "龙社区", value: "320130"),
        WyCascaderData(text: "武隆区", value: "320110"),
      ])
    ]),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: WyButton(
          text: 'WyCascader',
          onClick: () {
            WyCascader.show(context,
                value: value, activeColor: Colors.blue, options: options,
                fisinsh: (val, selectedOptions, tabIndex, callback) {
              value = selectedOptions;
            }, change: (val, selectedOptions, tabIndex, callback) {
              if (val == '330100') {
                Timer(Duration(seconds: 3), () {
                  options[0].children[0].children = [
                    WyCascaderData(text: "西湖区", value: "310120"),
                    WyCascaderData(text: "萧山区", value: "310130"),
                    WyCascaderData(text: "滨江区", value: "310110"),
                  ];
                  callback();
                });
              }
            });
          },
        ),
      ),
    );
  }
}

```

**参数说明**

|参数 |   说明  | 类型  |  默认值 |
| ------------ | ------------ | ------------ | ------------ |
|title |   顶部标题  | String  | 请选择所在地区  |
|value |   选中项的值  | List<WyCascaderData>  | null  |
|options |   可选项数据源  | List<WyCascaderData>  | null  |
|placeholder |   未选中时的提示文案  | String  | 请选择  |
|activeColor |   选中状态的高亮颜色  | Color  | Colors.red  |
|closeable |   是否显示关闭图标  | bool  | true  |
|fisinsh |   全部选项选择完成后触发  | CascaderEvent  | null  |
|change |   选中项变化时触发  | CascaderEvent  | null  |
|close |   点击关闭图标时触发  | Function  | null  |




































































































