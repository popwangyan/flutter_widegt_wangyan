// ignore: library_names
library wy_lazyLoad;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/gestures.dart' show DragStartBehavior;

typedef OffsetFunction = Offset Function();

typedef FilterFunction = String Function(String str);

final ScrollController kscrollcontroller = ScrollController();

class InheritedProvider extends InheritedWidget {
  InheritedProvider({@required this.data, Widget child}) : super(child: child);

  final WyLazyLoadData data;

  @override
  bool updateShouldNotify(InheritedProvider old) {
    return true;
  }
}

class WyLazyLoadScrollView extends StatelessWidget {
  const WyLazyLoadScrollView(
      {Key key,
      this.scrollParams = const ScrollParams(),
      this.child,
      this.loadingWidget,
      this.errorWidget,
      this.preload = 20,
      this.attempt = 3,
      this.filter})
      : assert(preload != null && preload >= 0),
        assert(attempt != null && preload >= 0),
        super(key: key);

  final ScrollParams scrollParams;
  final Widget child;
  final Widget loadingWidget;
  final Widget errorWidget;
  final double preload;
  final int attempt;
  final FilterFunction filter;

  @override
  Widget build(BuildContext context) {
    ScrollController _controller =
        this.scrollParams.controller ?? kscrollcontroller;
    return SingleChildScrollView(
      controller: _controller,
      scrollDirection: this.scrollParams.scrollDirection,
      reverse: this.scrollParams.reverse,
      padding: this.scrollParams.padding,
      physics: this.scrollParams.physics,
      primary: this.scrollParams.primary,
      dragStartBehavior: this.scrollParams.dragStartBehavior,
      child: InheritedProvider(
        data: WyLazyLoadData(
            controller: _controller,
            scrollDirection: this.scrollParams.scrollDirection,
            offsetFunction: () {
              RenderBox _box = context.findRenderObject();
              return _box.localToGlobal(Offset.zero);
            },
            loadingWidget: loadingWidget,
            errorWidget: errorWidget,
            preload: preload,
            attempt: attempt,
            filter: filter),
        child: child,
      ),
    );
  }

  static WyLazyLoadData of(BuildContext context) {
    InheritedProvider provider = context
        .getElementForInheritedWidgetOfExactType<InheritedProvider>()
        .widget;
    return provider.data;
  }
}

class WyLazyLoadData {
  WyLazyLoadData(
      {this.controller,
      this.offsetFunction,
      this.scrollDirection,
      this.loadingWidget,
      this.errorWidget,
      this.preload = 20,
      this.attempt,
      this.filter});
  final ScrollController controller;
  final OffsetFunction offsetFunction;
  final Axis scrollDirection;
  final Widget loadingWidget;
  final Widget errorWidget;
  final double preload;
  final int attempt;
  final FilterFunction filter;
}

class ScrollParams {
  const ScrollParams({
    this.controller,
    this.scrollDirection = Axis.vertical, //滚动方向，默认是垂直方向
    this.reverse = false,
    this.padding,
    this.physics,
    this.primary,
    this.dragStartBehavior = DragStartBehavior.start,
  });

  final ScrollController controller;
  final Axis scrollDirection;
  final bool reverse;
  final EdgeInsets padding;
  final ScrollPhysics physics;
  final bool primary;
  final DragStartBehavior dragStartBehavior;
}
