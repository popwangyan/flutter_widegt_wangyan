library wy_popup;

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

part 'style.dart';

enum PopupPosition { left, right, top, bottom }

enum PopupCloseIconPosition { topLeft, topRight, bottomLeft, bottomRight }

class PopupPage<T> extends PopupRoute<T> {
  PopupPage({
    this.overlay,
    this.position,
    this.closeOnClickOverlay,
    this.overlayColor,
    this.round,
    this.curve,
    this.closeIcon,
    this.height,
    this.width,
    this.duration,
    this.builder,
    this.iconPosition,
    this.style,
  });

  final bool overlay;
  final PopupPosition position;
  final bool closeOnClickOverlay;
  final bool round;
  final WidgetBuilder builder;
  final Duration duration;
  final Color overlayColor;
  final Widget closeIcon;
  final Curve curve;
  final double height;
  final double width;
  final PopupCloseIconPosition iconPosition;
  final WyPopupStyle style;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    double _width = width ?? MediaQuery.of(context).size.width;
    double _height = height ?? MediaQuery.of(context).size.height;
    return PopupContent(
        animation: animation,
        builder: builder,
        height: _height,
        width: _width,
        round: round,
        closeIcon: closeIcon,
        position: position,
        iconPosition: iconPosition,
        style: style);
  }

  @override
  Color get barrierColor =>
      overlay ? overlayColor : Colors.black.withOpacity(0.01);

  @override
  bool get barrierDismissible => closeOnClickOverlay;

  @override
  String get barrierLabel => null;

  @override
  Duration get transitionDuration => Duration(milliseconds: 300);

  AnimationController _animationController;

  @override
  AnimationController createAnimationController() {
    assert(_animationController == null);
    _animationController = AnimationController(
        duration: transitionDuration, vsync: navigator.overlay);
    return _animationController;
  }

  @override
  Animation<double> createAnimation() {
    CurvedAnimation _curve =
        CurvedAnimation(parent: _animationController, curve: curve);
    return Tween(begin: 0.0, end: 1.0).animate(_curve);
  }
}

// ignore: must_be_immutable
class PopupLayout extends StatelessWidget {
  PopupLayout({
    @required this.builder,
    @required this.animation,
    this.closeIcon,
  });

  final Animation animation;
  final WidgetBuilder builder;
  final Widget closeIcon;

  Offset get _offset => null;

  Positioned get _iconWidget => null;

  Alignment get _alignment => null;

  double get width => null;

  double get height => null;

  BoxDecoration get _boxDecoration => null;

  WyPopupStyle get _theme => null;

  double viewPortWidth;
  double viewPortHeight;

  @override
  Widget build(BuildContext context) {
    viewPortWidth = MediaQuery.of(context).size.width;
    viewPortHeight = MediaQuery.of(context).size.height;
    return Container(
      alignment: _alignment,
      child: AnimatedBuilder(
        animation: animation,
        builder: (context, child) {
          return Transform.translate(
            offset: _offset,
            child: ClipRRect(
              borderRadius: _boxDecoration.borderRadius,
              child: Container(
                height: height,
                width: width,
                color: _boxDecoration.color,
                child: Material(
                  child: Stack(
                    children: <Widget>[
                      builder(context),
                      _iconWidget,
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

// ignore: must_be_immutable
class PopupContent extends PopupLayout {
  PopupContent({
    this.builder,
    this.animation,
    this.position = PopupPosition.top,
    this.iconPosition,
    this.width,
    this.height,
    this.closeIcon,
    this.round,
    this.style,
  }) : super(builder: builder, animation: animation, closeIcon: closeIcon);

  final WidgetBuilder builder;
  final Animation animation;
  final PopupPosition position;
  final bool round;
  final double width;
  final double height;
  final Widget closeIcon;
  final PopupCloseIconPosition iconPosition;
  final WyPopupStyle style;

  @override
  Offset get _offset {
    Offset offset;
    switch (position) {
      case PopupPosition.left:
        offset = Offset(-width * (1 - animation.value), 0.0);
        break;
      case PopupPosition.right:
        offset = Offset(viewPortWidth - width * animation.value, 0.0);
        break;
      case PopupPosition.bottom:
        offset = Offset(0.0, viewPortHeight - height * animation.value);
        break;
      case PopupPosition.top:
        offset = Offset(0.0, -height * (1 - animation.value));
        break;
    }
    return offset;
  }

  @override
  Positioned get _iconWidget {
    Positioned iconWidget;
    switch (iconPosition) {
      case PopupCloseIconPosition.topLeft:
        iconWidget = Positioned(
          child: closeIcon,
          top: 10,
          left: 10,
        );
        break;
      case PopupCloseIconPosition.topRight:
        iconWidget = Positioned(
          child: closeIcon,
          top: 10,
          right: 10,
        );
        break;
      case PopupCloseIconPosition.bottomLeft:
        iconWidget = Positioned(
          child: closeIcon,
          left: 10,
          bottom: 10,
        );
        break;
      case PopupCloseIconPosition.bottomRight:
        iconWidget = Positioned(
          child: closeIcon,
          bottom: 10,
          right: 10,
        );
        break;
      default:
    }
    return iconWidget;
  }

  @override
  Alignment get _alignment {
    Alignment alignment;
    switch (position) {
      case PopupPosition.left:
        alignment = Alignment.centerLeft;
        break;
      case PopupPosition.right:
        alignment = Alignment.centerLeft;
        break;
      case PopupPosition.bottom:
        alignment = Alignment.topCenter;
        break;
      case PopupPosition.top:
        alignment = Alignment.topCenter;
        break;
    }
    return alignment;
  }

  @override
  BoxDecoration get _boxDecoration {
    BorderRadius _raduis;
    if (!round)
      return BoxDecoration(
          color: _theme.primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(0.0)));
    switch (position) {
      case PopupPosition.left:
        _raduis = BorderRadius.only(
            topRight: Radius.circular(_theme.popupRoundBorderRadius),
            bottomRight: Radius.circular(_theme.popupRoundBorderRadius));
        break;
      case PopupPosition.right:
        _raduis = BorderRadius.only(
            topLeft: Radius.circular(_theme.popupRoundBorderRadius),
            bottomLeft: Radius.circular(_theme.popupRoundBorderRadius));
        break;
      case PopupPosition.bottom:
        _raduis = BorderRadius.only(
            topRight: Radius.circular(_theme.popupRoundBorderRadius),
            topLeft: Radius.circular(_theme.popupRoundBorderRadius));
        break;
      case PopupPosition.top:
        _raduis = BorderRadius.only(
            bottomLeft: Radius.circular(_theme.popupRoundBorderRadius),
            bottomRight: Radius.circular(_theme.popupRoundBorderRadius));
        break;
    }
    return BoxDecoration(color: _theme.primaryColor, borderRadius: _raduis);
  }

  @override
  WyPopupStyle get _theme {
    return style;
  }
}

class WyPopup {
  /// 调用弹出层的方法
  /// params{bool overlay} 是否显示遮罩层 默认值为true
  /// params{Color overlayColor}  弹出层空白区域的颜色如果为null，则为透明色；
  /// params{PopupPosition position}  弹出层的位置，可选值为top, bottom, right, left;
  /// params{bool closeOnClickOverlay} 是否在点击遮罩层后关闭 默认为true
  /// params{bool closeable} 是否显示关闭图标 默认为false
  /// params{IconData closeIcon} 关闭按钮的widget 默认值为 icons.cancel
  /// params{PopupCloseIconPosition iconPosition} 关闭按钮的位置 默认值为PopupCloseIconPosition.topRight
  /// params{WidgetBuilder closeIconBuilder} 关闭按钮的构造方法，优先级高于closeIcon
  /// params{Curve curve} 动画类名, 动画的运动曲线
  /// params{double height} 弹出层的高度 当弹出层的位置position为left或者right时，该值无效；
  /// params{double width}  弹出层的宽带
  /// params{bool round} 是否显示圆角弹框
  /// params{WyPopupStyle style} 弹出层的主题，可以设置弹框的背景颜色和width和height，close图标的样式。
  /// params{WidgetBuilder builder}  弹出层的内容区域
  static Future<T> showSheet<T>(BuildContext context,
      {bool overlay = true,
      PopupPosition position = PopupPosition.bottom,
      bool closeOnClickOverlay = true,
      bool round = false,
      Curve curve = Curves.ease,
      IconData closeIcon = Icons.cancel,
      WidgetBuilder closeIconBuilder,
      bool closeable = false,
      PopupCloseIconPosition iconPosition = PopupCloseIconPosition.topRight,
      double height,
      double width,
      Color overlayColor,
      WyPopupStyle style,
      @required WidgetBuilder builder,
      Function close}) {
    WyPopupStyle _theme = style != null
        ? WyPopupStyle.defaultTheme.minxiWith(style)
        : WyPopupStyle.defaultTheme;

    return Navigator.of(context).push(PopupPage<T>(
        height:
            position == PopupPosition.left || position == PopupPosition.right
                ? null
                : height ?? _theme.height,
        width: position == PopupPosition.top || position == PopupPosition.bottom
            ? null
            : width ?? _theme.width,
        position: position,
        overlayColor: overlayColor ?? Colors.black.withOpacity(0.7),
        overlay: overlay,
        closeOnClickOverlay: true,
        iconPosition: iconPosition,
        curve: curve,
        closeIcon: closeable
            ? closeIconBuilder != null
                ? closeIconBuilder(context)
                : Icon(
                    closeIcon,
                    size: _theme.iconSize,
                    color: _theme.iconColor,
                  )
            : Container(),
        round: round,
        style: _theme,
        builder: (context) {
          return WillPopScope(
              child: builder(context),
              onWillPop: () {
                close();
                return Future.value(closeOnClickOverlay);
              });
        }));
  }
}
