part of wy_popup;

class WyPopupStyle {
  const WyPopupStyle(
      {this.primaryColor,
      this.iconSize,
      this.iconColor,
      this.width,
      this.popupRoundBorderRadius,
      this.height});

  const WyPopupStyle.row(
      {@required this.iconColor,
      @required this.iconSize,
      @required this.width,
      @required this.height,
      @required this.popupRoundBorderRadius,
      @required this.primaryColor})
      : assert(iconColor != null),
        assert(iconSize != null),
        assert(primaryColor != null),
        assert(width != null),
        assert(height != null),
        assert(popupRoundBorderRadius != null);

  final Color primaryColor;
  final double iconSize;
  final Color iconColor;
  final double width;
  final double height;
  final double popupRoundBorderRadius;

  WyPopupStyle copyWith(Color primaryColor, double iconSize, Color iconColor,
      double popupRoundBorderRadius, double width, double height) {
    return WyPopupStyle.row(
        iconColor: iconColor ?? this.iconColor,
        iconSize: iconSize ?? this.iconSize,
        primaryColor: primaryColor ?? this.primaryColor,
        popupRoundBorderRadius:
            popupRoundBorderRadius ?? this.popupRoundBorderRadius,
        width: width ?? this.width,
        height: height ?? this.height);
  }

  WyPopupStyle minxiWith(WyPopupStyle theme) {
    return WyPopupStyle.row(
        iconColor: theme.iconColor ?? this.iconColor,
        iconSize: theme.iconSize ?? this.iconSize,
        primaryColor: theme.primaryColor ?? this.primaryColor,
        popupRoundBorderRadius:
            theme.popupRoundBorderRadius ?? this.popupRoundBorderRadius,
        width: theme.width ?? this.width,
        height: theme.height ?? this.height);
  }

  static const WyPopupStyle defaultTheme = const WyPopupStyle.row(
      iconColor: Color.fromRGBO(200, 201, 204, 1),
      iconSize: 22.0,
      primaryColor: Colors.white,
      popupRoundBorderRadius: 16,
      width: 260,
      height: 360);
}
