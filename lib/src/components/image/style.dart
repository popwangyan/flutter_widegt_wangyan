part of wy_image;

class WyImageStyle {
  WyImageStyle(
      {this.imagePlaceholderBackgroundColor,
      this.imageLoadingIconSize,
      this.imageLoadingIconColor,
      this.imageErrorIconSize,
      this.imageErrorIconColor});

  const WyImageStyle.row({
    @required this.imageErrorIconColor,
    @required this.imageErrorIconSize,
    @required this.imageLoadingIconColor,
    @required this.imageLoadingIconSize,
    @required this.imagePlaceholderBackgroundColor,
  })  : assert(imageErrorIconColor != null),
        assert(imageErrorIconSize != null),
        assert(imageLoadingIconColor != null),
        assert(imageLoadingIconSize != null),
        assert(imagePlaceholderBackgroundColor != null);

  final Color imagePlaceholderBackgroundColor;
  final double imageLoadingIconSize;
  final Color imageLoadingIconColor;
  final double imageErrorIconSize;
  final Color imageErrorIconColor;

  WyImageStyle copyWith(
      {Color imagePlaceholderBackgroundColor,
      double imageLoadingIconSize,
      Color imageLoadingIconColor,
      double imageErrorIconSize,
      Color imageErrorIconColor}) {
    return WyImageStyle.row(
        imageErrorIconColor: imageErrorIconColor ?? this.imageErrorIconColor,
        imageErrorIconSize: imageErrorIconSize ?? this.imageErrorIconSize,
        imageLoadingIconColor:
            imageLoadingIconColor ?? this.imageLoadingIconColor,
        imageLoadingIconSize: imageLoadingIconSize ?? this.imageLoadingIconSize,
        imagePlaceholderBackgroundColor: imagePlaceholderBackgroundColor ??
            this.imagePlaceholderBackgroundColor);
  }

  WyImageStyle minxiWith(WyImageStyle style) {
    return WyImageStyle.row(
        imageErrorIconColor:
            style.imageErrorIconColor ?? this.imageErrorIconColor,
        imageErrorIconSize: style.imageErrorIconSize ?? this.imageErrorIconSize,
        imageLoadingIconColor:
            style.imageLoadingIconColor ?? this.imageLoadingIconColor,
        imageLoadingIconSize:
            style.imageLoadingIconSize ?? this.imageLoadingIconSize,
        imagePlaceholderBackgroundColor:
            style.imagePlaceholderBackgroundColor ??
                this.imagePlaceholderBackgroundColor);
  }

  static const WyImageStyle defaultStyle = const WyImageStyle.row(
      imageErrorIconColor: Color.fromRGBO(220, 222, 224, 1),
      imageErrorIconSize: 48,
      imageLoadingIconColor: Color.fromRGBO(220, 222, 224, 1),
      imageLoadingIconSize: 48,
      imagePlaceholderBackgroundColor: Color.fromRGBO(247, 248, 250, 1));
}
