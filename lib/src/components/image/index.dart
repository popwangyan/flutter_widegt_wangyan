library wy_image;

import 'dart:async';

import 'package:flutter/material.dart';
import '../lazyLoad/index.dart';
part 'style.dart';

enum WyImageState { loading, error, success }

const double preload = 20;

class WyImage extends StatefulWidget {
  WyImage(
      {Key key,
      @required this.src,
      @required this.width,
      @required this.height,
      this.fit = BoxFit.fill,
      this.radius = const Radius.circular(0.0),
      this.round = false,
      this.lazyLoad = false,
      this.showError = true,
      this.showLoading = true,
      this.errorIcon = Icons.broken_image,
      this.loadingIcon = Icons.image,
      this.click,
      this.load,
      this.error,
      this.loadingBuilder,
      this.errorBuilder,
      this.progress,
      WyImageStyle style})
      : assert(src != null),
        assert(width != null),
        assert(height != null),
        style = style == null
            ? WyImageStyle.defaultStyle
            : WyImageStyle.defaultStyle.minxiWith(style),
        super(key: key);

  final String src;
  final double width;
  final double height;
  final BoxFit fit;
  final Radius radius;
  final bool round;
  final bool lazyLoad;
  final bool showError;
  final bool showLoading;
  final IconData errorIcon;
  final IconData loadingIcon;

  final Function click;
  final Function load;
  final Function error;
  final ImageChunkListener progress;

  final WidgetBuilder loadingBuilder;
  final WidgetBuilder errorBuilder;

  final WyImageStyle style;

  @override
  _WyImageState createState() => _WyImageState();
}

class _WyImageState extends State<WyImage> {
  @override
  Widget build(BuildContext context) {
    Widget result;
    switch (_state) {
      case WyImageState.loading:
        result = loading();
        break;
      case WyImageState.error:
        result = error();
        break;
      case WyImageState.success:
        result = success();
        break;
      default:
        result = loading();
    }

    if (widget.round)
      return ClipOval(
        child: result,
      );

    return ClipRRect(
      child: result,
      borderRadius: BorderRadius.all(widget.radius),
    );
  }

  Widget container(Widget child) {
    return Container(
      width: widget.width,
      height: widget.height,
      color: widget.style.imagePlaceholderBackgroundColor,
      constraints:
          BoxConstraints(maxWidth: widget.width, maxHeight: widget.height),
      child: child ?? Container(),
    );
  }

  Widget loading() {
    if (!widget.showLoading) {
      return container(null);
    }

    if (_wyLazyLoadData.loadingWidget != null) {
      return container(_wyLazyLoadData.loadingWidget);
    }

    return container(widget.loadingBuilder == null
        ? Icon(
            Icons.image,
            size: widget.style.imageLoadingIconSize,
            color: widget.style.imageLoadingIconColor,
          )
        : widget.loadingBuilder(context));
  }

  Widget error() {
    if (!widget.showError) {
      return container(null);
    }

    if (_wyLazyLoadData.errorWidget != null) {
      return container(_wyLazyLoadData.errorWidget);
    }

    return container(widget.errorBuilder == null
        ? Icon(
            Icons.broken_image,
            size: widget.style.imageErrorIconSize,
            color: widget.style.imageErrorIconColor,
          )
        : widget.errorBuilder(context));
  }

  Widget success() {
    return Image(
      image: _imageProvider,
      width: widget.width,
      height: widget.height,
      fit: widget.fit,
    );
  }

  WyImageState _state = WyImageState.loading;
  ImageStream _stream;
  ImageStreamListener _imageStreamListener;
  int attempt = 0;
  FilterFunction filterFunction;

  ImageProvider get _imageProvider {
    String url =
        filterFunction != null ? filterFunction(widget.src) : widget.src;
    return url.startsWith("http") ? NetworkImage(url) : AssetImage(url);
  }

  Future<bool> load() async {
    await Future.delayed(Duration(milliseconds: 300));
    Completer<bool> _completer = Completer<bool>();
    _state = WyImageState.loading;
    _stream = _imageProvider.resolve(ImageConfiguration.empty);
    _imageStreamListener =
        ImageStreamListener((ImageInfo imageInfo, bool flag) {
      _stream.removeListener(_imageStreamListener);
      _completer.complete(true);
      if (widget.load != null) widget.load();
      setState(() {
        _state = WyImageState.success;
      });
    }, onError: (dynamic exception, StackTrace stackTrace) {
      _stream.removeListener(_imageStreamListener);
      _completer.completeError(false);
      if (widget.error != null) widget.error();
      if (attempt > 0) {
        --attempt;
        load();
      } else {
        setState(() {
          _state = WyImageState.error;
        });
      }
    }, onChunk: (ImageChunkEvent chunkEvent) {});

    _stream.addListener(_imageStreamListener);
    return _completer.future;
  }

  WyLazyLoadData get _wyLazyLoadData => WyLazyLoadScrollView.of(context);

  double get _position {
    RenderBox box = context.findRenderObject();
    double distance = _wyLazyLoadData.scrollDirection == Axis.vertical
        ? _wyLazyLoadData.offsetFunction().dy
        : _wyLazyLoadData.offsetFunction().dx;
    Offset offset = box.localToGlobal(Offset.zero).translate(0, -distance);
    return _wyLazyLoadData.scrollDirection == Axis.vertical
        ? offset.dy
        : offset.dx;
  }

  bool get _canload {
    double distance = _wyLazyLoadData.controller.position.viewportDimension;
    if (_position <= distance + _wyLazyLoadData.preload) return true;
    return false;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      if (!widget.lazyLoad) {
        load();
        return;
      }
      attempt = _wyLazyLoadData.attempt;
      filterFunction = _wyLazyLoadData.filter;
      if (_canload) {
        load();
        return;
      }
      if (_wyLazyLoadData.controller != null) {
        _wyLazyLoadData.controller.addListener(listener);
      }
    });
  }

  void listener() {
    if (_canload) {
      _wyLazyLoadData.controller.removeListener(listener);
      load();
    }
  }

  @override
  void didUpdateWidget(WyImage oldWidget) {
    super.didUpdateWidget(oldWidget);
    // load();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
