part of wy_calendar;

class WyCalendarStyle {
  WyCalendarStyle(
      {this.backgroundColor,
      this.popupHeight,
      this.headerBoxShadow,
      this.headerTitleHeight,
      this.headerTitleFontSize,
      this.headerSubtitleFontSize,
      this.weekdaysHeight,
      this.weekdaysFontSize,
      this.monthTitleFontSize,
      this.monthMarkColor,
      this.monthMarkFontSize,
      this.dayHeight,
      this.dayFontSize,
      this.rangeEdgeColor});

  WyCalendarStyle.row(
      {@required this.backgroundColor,
      @required this.popupHeight,
      @required this.headerBoxShadow,
      @required this.headerTitleHeight,
      @required this.headerTitleFontSize,
      @required this.headerSubtitleFontSize,
      @required this.weekdaysHeight,
      @required this.weekdaysFontSize,
      @required this.monthTitleFontSize,
      @required this.monthMarkColor,
      @required this.monthMarkFontSize,
      @required this.dayHeight,
      @required this.dayFontSize,
      @required this.rangeEdgeColor})
      : assert(backgroundColor != null),
        assert(popupHeight != null),
        assert(headerBoxShadow != null),
        assert(headerTitleHeight != null),
        assert(headerTitleFontSize != null),
        assert(headerSubtitleFontSize != null),
        assert(weekdaysHeight != null),
        assert(weekdaysFontSize != null),
        assert(monthTitleFontSize != null),
        assert(monthMarkColor != null),
        assert(monthMarkFontSize != null),
        assert(dayHeight != null),
        assert(dayFontSize != null),
        assert(rangeEdgeColor != null);

  final Color backgroundColor;
  final String popupHeight;
  final List<BoxShadow> headerBoxShadow;
  final double headerTitleHeight;
  final double headerTitleFontSize;
  final double headerSubtitleFontSize;
  final double weekdaysHeight;
  final double weekdaysFontSize;
  final double monthTitleFontSize;
  final Color monthMarkColor;
  final double monthMarkFontSize;
  final double dayHeight;
  final double dayFontSize;
  final Color rangeEdgeColor;

  WyCalendarStyle copyWith(
      {Color backgroundColor,
      String popupHeight,
      List<BoxShadow> headerBoxShadow,
      double headerTitleHeight,
      double headerTitleFontSize,
      double headerSubtitleFontSize,
      double weekdaysHeight,
      double weekdaysFontSize,
      double monthTitleFontSize,
      Color monthMarkColor,
      double monthMarkFontSize,
      double dayHeight,
      double dayFontSize,
      Color rangeEdgeColor}) {
    return WyCalendarStyle.row(
        backgroundColor: backgroundColor ?? this.backgroundColor,
        popupHeight: popupHeight ?? this.popupHeight,
        headerBoxShadow: headerBoxShadow ?? this.headerBoxShadow,
        headerTitleHeight: headerTitleHeight ?? this.headerTitleHeight,
        headerTitleFontSize: headerTitleFontSize ?? this.headerTitleFontSize,
        headerSubtitleFontSize:
            headerSubtitleFontSize ?? this.headerSubtitleFontSize,
        weekdaysHeight: weekdaysHeight ?? this.weekdaysHeight,
        weekdaysFontSize: weekdaysFontSize ?? this.weekdaysFontSize,
        monthTitleFontSize: monthTitleFontSize ?? this.monthTitleFontSize,
        monthMarkColor: monthMarkColor ?? this.monthMarkColor,
        monthMarkFontSize: monthMarkFontSize ?? this.monthMarkFontSize,
        dayHeight: dayHeight ?? this.dayHeight,
        dayFontSize: dayFontSize ?? this.dayFontSize,
        rangeEdgeColor: rangeEdgeColor ?? this.rangeEdgeColor);
  }

  WyCalendarStyle minxiWith(WyCalendarStyle style) {
    return WyCalendarStyle.row(
        backgroundColor: style.backgroundColor ?? this.backgroundColor,
        popupHeight: style.popupHeight ?? this.popupHeight,
        headerBoxShadow: style.headerBoxShadow ?? this.headerBoxShadow,
        headerTitleHeight: style.headerTitleHeight ?? this.headerTitleHeight,
        headerTitleFontSize:
            style.headerTitleFontSize ?? this.headerTitleFontSize,
        headerSubtitleFontSize:
            style.headerSubtitleFontSize ?? this.headerSubtitleFontSize,
        weekdaysHeight: style.weekdaysHeight ?? this.weekdaysHeight,
        weekdaysFontSize: style.weekdaysFontSize ?? this.weekdaysFontSize,
        monthTitleFontSize: style.monthTitleFontSize ?? this.monthTitleFontSize,
        monthMarkColor: style.monthMarkColor ?? this.monthMarkColor,
        monthMarkFontSize: style.monthMarkFontSize ?? this.monthMarkFontSize,
        dayHeight: style.dayHeight ?? this.dayHeight,
        dayFontSize: style.dayFontSize ?? this.dayFontSize,
        rangeEdgeColor: style.rangeEdgeColor ?? this.rangeEdgeColor);
  }

  static WyCalendarStyle defaultStyle = WyCalendarStyle.row(
      backgroundColor: Colors.white,
      popupHeight: "80%",
      headerBoxShadow: [
        BoxShadow(
            color: Color.fromRGBO(125, 126, 128, 0.16),
            offset: Offset(0.2, 10),
            blurRadius: 10,
            spreadRadius: 0)
      ],
      headerTitleHeight: 44,
      headerTitleFontSize: 16,
      headerSubtitleFontSize: 14,
      weekdaysHeight: 30,
      weekdaysFontSize: 12,
      monthTitleFontSize: 14,
      monthMarkColor: Color.fromRGBO(242, 243, 245, 0.8),
      monthMarkFontSize: 160,
      dayHeight: 64,
      dayFontSize: 16,
      rangeEdgeColor: Colors.white);
}
