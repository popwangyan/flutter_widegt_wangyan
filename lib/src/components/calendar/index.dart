library wy_calendar;

import 'package:flutter/material.dart';
import 'package:flutter_widegt_wangyan/src/utils.dart';
import '../button/index.dart';
import '../popup/index.dart';
import '../toast/index.dart';

part 'modal.dart';

part 'style.dart';

typedef CalendarFormatter = Date Function(Date date);

typedef CalendarSelect = void Function(List<DateTime>);

typedef CalendarMonthShow = void Function(DateTime date);

// ignore: must_be_immutable
class WyCalendar extends StatelessWidget {
  WyCalendar(
      {this.height = 510,
      this.width,
      this.rowHeight = 64,
      this.firstDayOfWeek = 1,
      this.type,
      this.title = "日历",
      this.showConfirm = true,
      this.showMark = true,
      this.showTitle = true,
      this.showSubtitle = true,
      this.readonly = false,
      this.confirmText = "确定",
      this.confirmDisabledText = "确定",
      this.formatter,
      this.defaultDate,
      this.maxRange,
      this.rangePrompt = "选择天数不能超过 xx 天",
      this.select,
      this.monthShow,
      this.style,
      this.controller,
      this.titleBuilder,
      this.footerBuilder,
      Color color = Colors.red,
      DateTime minDate,
      DateTime maxDate})
      : assert(height != null && height >= 510),
        assert(rowHeight != null && rowHeight >= 45),
        assert(firstDayOfWeek != null &&
            firstDayOfWeek >= 0 &&
            firstDayOfWeek <= 6),
        color = color,
        assert(maxRange == null || maxRange > 0),
        assert(rangePrompt != null && rangePrompt.indexOf("xx") > -1) {
    _minDate = minDate ??
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    _maxDate = maxDate ?? DateTime.now().add(Duration(days: 365));
    assert(_maxDate.isAfter(_minDate));
    if (type == CalenderType.range) {
      assert(defaultDate == null || defaultDate.length == 2);
      if (defaultDate != null && defaultDate.length == 2) {
        assert(defaultDate[0].isBefore(defaultDate[1]));
      }
    }
    if (type == CalenderType.single) {
      assert(defaultDate == null || defaultDate.length == 1);
    }
  }

  final double height;
  final double width;
  final double rowHeight;
  final int firstDayOfWeek;
  DateTime _minDate;
  DateTime _maxDate;
  final CalenderType type;
  final List<DateTime> defaultDate;
  final Color color;
  final bool showConfirm;
  final bool showMark;
  final bool showTitle;
  final String confirmText;
  final String confirmDisabledText;
  final CalendarFormatter formatter;
  final String title;
  final WidgetBuilder titleBuilder;
  final WidgetBuilder footerBuilder;
  final bool showSubtitle;
  final bool readonly;
  final int maxRange;
  final String rangePrompt;
  final CalendarSelect select;
  final CalendarMonthShow monthShow;
  final WyCalendarStyle style;
  final WyCalenderController controller;

  @override
  Widget build(BuildContext context) {
    WyCalendarStyle _style = WyCalendar.getStyle(style);
    return Container(
      width: double.infinity,
      child: CalendarBox(
          minDate: _minDate,
          maxDate: _maxDate,
          type: type,
          color: color,
          titleBuilder: titleBuilder,
          footerBuilder: footerBuilder,
          rowHeight: rowHeight,
          showConfirm: showConfirm,
          showMark: showMark,
          showTitle: showTitle,
          title: title,
          rangePrompt: rangePrompt,
          maxRange: maxRange,
          readonly: readonly,
          showSubtitle: showSubtitle,
          confirmText: confirmText,
          confirmDisabledText: confirmDisabledText,
          defaultDate: defaultDate,
          formatter: formatter,
          firstDayOfWeek: firstDayOfWeek,
          select: select,
          monthShow: monthShow,
          controller: controller,
          style: _style),
    );
  }

  static Future<List<DateTime>> show(BuildContext context,
      {DateTime minDate,
      DateTime maxDate,
      CalenderType type = CalenderType.single,
      Color color = Colors.red,
      bool closeOnClickOverlay = true,
      bool showConfirm = true,
      bool showMark = true,
      bool showTitle = true,
      bool showSubtitle = true,
      bool readonly = false,
      int maxRange,
      bool round = false,
      WidgetBuilder titleBuilder,
      String rangePrompt = "选择天数不能超过 xx 天",
      String title = "日历",
      String confirmText = "确定",
      String confirmDisabledText = "确定",
      List<DateTime> defaultDate,
      double rowHeight = 64,
      CalendarFormatter formatter,
      int firstDayOfWeek = 1,
      PopupPosition position = PopupPosition.bottom,
      CalendarSelect select,
      CalendarMonthShow monthShow,
      WyCalenderController controller,
      WyCalendarStyle style}) {
    WyCalendarStyle _style = WyCalendar.getStyle(style);
    return WyPopup.showSheet(context,
        position: position,
        round: round,
        closeOnClickOverlay: closeOnClickOverlay,
        width: MediaQuery.of(context).size.width,
        height: Utils.getNumber(context, _style.popupHeight),
        builder: (context) {
      Widget content = Material(
        child: WyCalendar(
            color: color,
            maxDate: maxDate,
            minDate: minDate,
            type: type,
            title: title,
            readonly: readonly,
            showConfirm: showConfirm,
            showMark: showMark,
            showTitle: showTitle,
            showSubtitle: showSubtitle,
            confirmText: confirmText,
            titleBuilder: titleBuilder,
            confirmDisabledText: confirmDisabledText,
            defaultDate: defaultDate,
            rowHeight: rowHeight,
            firstDayOfWeek: firstDayOfWeek,
            formatter: formatter,
            maxRange: maxRange,
            select: select,
            controller: controller,
            monthShow: monthShow,
            rangePrompt: rangePrompt),
      );
      return position == PopupPosition.bottom
          ? content
          : SafeArea(child: content);
    });
  }

  static WyCalendarStyle getStyle(WyCalendarStyle style) {
    WyCalendarStyle _style = style != null
        ? WyCalendarStyle.defaultStyle.minxiWith(style)
        : WyCalendarStyle.defaultStyle;
    return _style;
  }
}

class CalendarBox extends StatefulWidget {
  CalendarBox({
    Key key,
    this.minDate,
    this.maxDate,
    this.type,
    this.color,
    this.showMark,
    this.showConfirm,
    this.confirmText,
    this.confirmDisabledText,
    this.defaultDate,
    this.rowHeight,
    this.formatter,
    this.firstDayOfWeek,
    this.showTitle,
    this.title,
    this.showSubtitle,
    this.readonly,
    this.maxRange,
    this.rangePrompt,
    this.select,
    this.monthShow,
    this.style,
    this.controller,
    this.titleBuilder,
    this.footerBuilder,
  }) : super(key: key);

  final CalenderType type;
  final Color color;
  final DateTime minDate;
  final DateTime maxDate;
  final List<DateTime> defaultDate;
  final double rowHeight;
  final bool showConfirm;
  final bool showMark;
  final bool showTitle;
  final String confirmText;
  final String confirmDisabledText;
  final CalendarFormatter formatter;
  final int firstDayOfWeek;
  final String title;
  final bool showSubtitle;
  final bool readonly;
  final int maxRange;
  final String rangePrompt;
  final CalendarSelect select;
  final CalendarMonthShow monthShow;
  final WyCalendarStyle style;
  final WyCalenderController controller;
  final WidgetBuilder titleBuilder;
  final WidgetBuilder footerBuilder;

  @override
  _CalendarBoxState createState() => _CalendarBoxState();
}

class _CalendarBoxState extends State<CalendarBox> {
  @override
  Widget build(BuildContext context) {
    viewPortWidth = MediaQuery.of(context).size.width;
    return Container(
      color: widget.style.backgroundColor,
      child: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          title(),
          week(),
          Expanded(
            child: Container(
              child: ListView.builder(
                controller: _controller,
                itemCount: _month.length,
                itemBuilder: (context, index) {
                  return MonthDetail(
                    rowHeight: widget.rowHeight,
                    viewPortWidth: viewPortWidth,
                    minDate: widget.minDate,
                    date: _month[index],
                    showMark: widget.showMark,
                    showTitle: index == 0 ? false : true,
                    type: widget.type,
                    selectedDateList: selectedDateList,
                    onClick: onClick,
                    color: widget.color,
                    firstDayOfWeek: widget.firstDayOfWeek,
                    formatter: widget.formatter,
                    style: widget.style,
                  );
                },
              ),
            ),
          ),
          Visibility(
            child: footer(),
            visible: widget.showConfirm,
          )
        ],
      ),
    );
  }

  /// 组件的title
  Widget title() {
    if (widget.titleBuilder != null) return widget.titleBuilder(context);
    return Container(
      height: widget.style.headerTitleHeight,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Center(
            child: Text(
              widget.showTitle ? widget.title : '',
              style: TextStyle(
                fontSize: widget.style.headerTitleFontSize,
              ),
            ),
          ),
          Positioned(
            child: IconButton(
                icon: Icon(
                  Icons.clear,
                  size: widget.style.headerTitleFontSize,
                  color: Colors.grey,
                ),
                highlightColor: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                }),
            right: 0.0,
          )
        ],
      ),
    );
  }

  Widget week() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: widget.style.backgroundColor,
        boxShadow: _boxShadow,
      ),
      child: Column(
        children: <Widget>[
          Visibility(
            child: SizedBox(
              height: 44.0,
              child: Center(
                child: Text(weekTime,
                    style: TextStyle(
                        fontSize: widget.style.headerSubtitleFontSize)),
              ),
            ),
            visible: widget.showSubtitle,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: weeks.map((item) {
              return Container(
                height: widget.style.weekdaysHeight,
                width: MediaQuery.of(context).size.width / 7,
                child: Center(
                  child: Text(
                    item,
                    style: TextStyle(fontSize: widget.style.weekdaysFontSize),
                  ),
                ),
              );
            }).toList(),
          )
        ],
      ),
    );
  }

  Widget footer() {
    if (widget.footerBuilder != null) return widget.footerBuilder(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 16),
      child: WyButton(
        color: widget.color,
        size: ButtonSize.normal,
        shape: ShapeType.round,
        block: true,
        disabled: disabled,
        text: buttonText,
        onClick: () {
          Navigator.of(context).pop(_selectedDateTimeList);
        },
      ),
    );
  }

  double viewPortWidth;
  WyCalenderController _controller;
  List<Date> _month = [];
  String weekTime;

  List<DateTime> selectedDateList = [];
  bool disabled = false;
  String buttonText = "确定";

  DateTime startTime;
  DateTime endTime;

  List<String> get weeks {
    List<String> _result = [];
    List<String> _part1 = _weeks.sublist(0, widget.firstDayOfWeek);
    List<String> _part2 = _weeks.sublist(widget.firstDayOfWeek);
    _result.addAll(_part2);
    _result.addAll(_part1);
    return _result;
  }

  double get boxHeight => widget.style.headerTitleHeight + 64 * 5.0;

  List<String> _weeks = ['日', '一', '二', '三', '四', '五', '六'];

  @override
  void initState() {
    super.initState();
    _controller = widget.controller ?? WyCalenderController();
    _controller.addListener(scrollListen);
    _controller.addListener(resetDate);
    _controller.addListener(scrollToDate);
    initTime();
    initValue();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.removeListener(scrollListen);
    _controller.removeListener(resetDate);
    _controller.removeListener(scrollToDate);
  }

  void resetDate() {
    if (_controller.times != null) {
      switch (widget.type) {
        case CalenderType.single:
          if (_controller.times.length > 0) {
            selectedDateList = [_controller.times[0]];
          } else {
            selectedDateList = widget.defaultDate ?? [];
          }
          break;
        case CalenderType.multiple:
          if (_controller.times.length > 0) {
            selectedDateList = _controller.times;
          } else {
            selectedDateList = widget.defaultDate ?? [];
          }
          break;
        default:
          if (_controller.times.length >= 2) {
            selectedDateList = [_controller.times[0], _controller.times[1]];
          } else {
            selectedDateList = [];
          }
      }
      setState(() {});
    }
  }

  void scrollToDate() {
    if (_controller.toDate != null) {
      if (_controller.toDate.isBefore(widget.minDate)) {
        _controller.animateTo(0,
            duration: Duration(microseconds: 200), curve: Curves.ease);
      }
      if (_controller.toDate.isAfter(widget.maxDate)) {
        _controller.animateTo(100000,
            duration: Duration(microseconds: 200), curve: Curves.ease);
      }

      int index = 0;
      DateTime start = widget.minDate;

      while (start.isBefore(_controller.toDate)) {
        index++;
        start = new DateTime(start.year, start.month + 1, start.day);
      }

      _controller.animateTo(boxHeight * index + widget.style.headerTitleHeight,
          duration: Duration(microseconds: 200), curve: Curves.ease);
    }
  }

  void scrollListen() {
    double scrollTop = _controller.offset;
    int index = (scrollTop / boxHeight).floor();
    setWeekTime(_month[index]);
    if (widget.monthShow != null) {
      widget.monthShow(_month[index].date);
    }
    setState(() {});
  }

  void setWeekTime(Date date) {
    weekTime =
        date.date.year.toString() + "年" + date.date.month.toString() + '月';
  }

  void initTime() {
    Date minDate = Date(date: widget.minDate);

    Date maxDate = Date(date: widget.maxDate);

    while (maxDate >= minDate) {
      Date time =
          Date(date: DateTime(minDate.date.year, minDate.date.month, 1));
      _month.add(time);
      minDate.setMonth(num: 1);
    }

    setWeekTime(_month[0]);
    setState(() {});
  }

  void initValue() {
    selectedDateList = widget.defaultDate ?? [];
  }

  void onClick(date) {
    if (widget.readonly) return;
    switch (widget.type) {
      case CalenderType.single:
        setSingleDate(date);
        break;
      case CalenderType.multiple:
        setMultipleDate(date);
        break;
      case CalenderType.range:
        setRangeDate(date);
        break;
    }

    disabled = _disabled;
    buttonText = _buttonText;

    setState(() {});
  }

  void setSingleDate(DateTime date) {
    if (selectedDateList.length == 0) {
      selectedDateList.add(date);
    } else {
      selectedDateList = [date];
    }
    if (widget.select != null) widget.select(_selectedDateTimeList);
  }

  void setMultipleDate(DateTime date) {
    if (selectedDateList.indexOf(date) > -1) {
      selectedDateList.remove(date);
    } else {
      if (widget.maxRange != null) {
        if (selectedDateList.length == widget.maxRange) {
          WyToast(context, message: _rangePrompt);
          return;
        }
      }
      selectedDateList.add(date);
      if (widget.select != null) widget.select(_selectedDateTimeList);
    }
  }

  void setRangeDate(DateTime date) {
    if (selectedDateList.indexOf(date) > -1) return;
    if (selectedDateList.length == 0 || selectedDateList.length == 2) {
      selectedDateList.clear();
      selectedDateList.add(date);
      if (widget.select != null) widget.select(_selectedDateTimeList);
    } else {
      DateTime startTime = selectedDateList[0];
      if (startTime.isAfter(date)) {
        selectedDateList.clear();
        selectedDateList.add(date);
        if (widget.select != null) widget.select(_selectedDateTimeList);
      } else {
        if (widget.maxRange != null) {
          DateTime _end = startTime.add(Duration(days: widget.maxRange - 1));
          if (date.isAfter(_end)) {
            selectedDateList.add(_end);
            while (_selectedDateTimeList.length < widget.maxRange &&
                date.isAfter(_end)) {
              _end = _end.add(Duration(days: 1));
              selectedDateList[1] = _end;
            }
            if (date.isAfter(_end)) {
              WyToast(context, message: _rangePrompt);
            }
            if (widget.select != null) widget.select(_selectedDateTimeList);
            return;
          }
        }
        selectedDateList.add(date);
        if (widget.select != null) widget.select(_selectedDateTimeList);
      }
    }
  }

  bool get _disabled {
    switch (widget.type) {
      case CalenderType.single:
      case CalenderType.multiple:
        return selectedDateList.length == 0;
        break;
      default:
        return selectedDateList.length != 2;
    }
  }

  String get _buttonText {
    return _disabled ? widget.confirmDisabledText : widget.confirmText;
  }

  String get _rangePrompt {
    return widget.rangePrompt.replaceAll("xx", widget.maxRange.toString());
  }

  List<BoxShadow> get _boxShadow {
    if (!_controller.hasClients || _controller.offset == 0) return [];
    return widget.style.headerBoxShadow;
  }

  List<DateTime> get _selectedDateTimeList {
    List<DateTime> result = [];
    if (widget.type == CalenderType.range && selectedDateList.length == 2) {
      DateTime startTime = selectedDateList[0];
      DateTime endTime = selectedDateList[1];

      while (startTime.isBefore(endTime) || startTime == endTime) {
        result.add(startTime);
        if (widget.formatter != null) {
          Date _myData = widget.formatter(Date(
              date: startTime,
              type: DayStyle(
                      time: startTime,
                      selectedDateList: selectedDateList,
                      type: widget.type)
                  .dayType));
          if (_myData.type == DayType.disabled) {
            result.remove(startTime);
          }
        }
        startTime = startTime.add(Duration(days: 1));
      }
    } else {
      result = selectedDateList;
    }
    return result;
  }
}

class MonthDetail extends StatelessWidget {
  MonthDetail({
    this.viewPortWidth,
    this.date,
    this.showTitle = true,
    this.onClick,
    this.type,
    this.selectedDateList,
    this.color,
    this.showMark,
    this.rowHeight,
    this.formatter,
    this.firstDayOfWeek,
    this.minDate,
    this.style,
  });

  final double viewPortWidth;
  final Date date;
  final bool showMark;
  final bool showTitle;
  final Function onClick;
  final CalenderType type;
  final List<DateTime> selectedDateList;
  final Color color;
  final double rowHeight;
  final CalendarFormatter formatter;
  final int firstDayOfWeek;
  final DateTime minDate;
  final WyCalendarStyle style;

  @override
  Widget build(BuildContext context) {
    String time =
        date.date.year.toString() + "年" + date.date.month.toString() + '月';
    int week = DateTime(date.date.year, date.date.month, 1).weekday;
    List<Date> days = List.generate(
        week == 7
            ? 0
            : (week - firstDayOfWeek) >= 0
                ? (week - firstDayOfWeek)
                : 7 + (week - firstDayOfWeek),
        (_) => Date());
    days.addAll(date.getMonthCount().map((item) {
      DateTime _time = DateTime(date.date.year, date.date.month, item);

      return Date(
          type: _time.isBefore(minDate) ? DayType.disabled : null,
          date: _time,
          text: item.toString());
    }).toList());
    return Container(
      child: Column(
        children: <Widget>[
          Visibility(
            child: SizedBox(
              width: double.infinity,
              height: style.headerTitleHeight,
              child: Center(
                child: Text(
                  time,
                  style: TextStyle(fontSize: style.monthTitleFontSize),
                ),
              ),
            ),
            visible: showTitle,
          ),
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Visibility(
                  visible: showMark,
                  child: Positioned(
                    child: Container(
                      child: Center(
                        child: Text(
                          date.date.month.toString(),
                          style: TextStyle(
                              fontSize: style.monthMarkFontSize,
                              color: style.monthMarkColor),
                        ),
                      ),
                    ),
                  )),
              Container(
                child: Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.start,
                  children: days.map((item) => day(item)).toList(),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget day(Date day) {
    if (day.date == null)
      return Container(
        width: (viewPortWidth / 7).floor().toDouble(),
        height: rowHeight,
      );

    DayStyle myDayStyle = DayStyle(
        time: day.date,
        selectedDateList: selectedDateList,
        type: type,
        color: color);
    Date _day = formatter == null
        ? Date(
            text: day.text,
            date: day.date,
            type: day.type == DayType.disabled
                ? DayType.disabled
                : myDayStyle.dayType,
            bottomInfo: myDayStyle.text)
        : formatter(Date(
            text: day.text,
            date: day.date,
            type: day.type == DayType.disabled
                ? DayType.disabled
                : myDayStyle.dayType,
            bottomInfo: myDayStyle.text));
    TextStyle _textStyle = myDayStyle.textStyle != null
        ? myDayStyle.textStyle.copyWith(fontSize: style.dayFontSize)
        : myDayStyle.textStyle;
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (_day.type == DayType.disabled) return;
        onClick(_day.date);
      },
      child: Container(
        width: (viewPortWidth / 7).floor().toDouble(),
        height: rowHeight,
        decoration:
            _day.type == DayType.disabled ? null : myDayStyle.boxDecoration,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
                top: 6,
                child: Text(
                  _day.topInfo ?? "",
                  style: _day.type == DayType.disabled
                      ? TextStyle(color: Colors.black38)
                      : myDayStyle.textStyle,
                )),
            Text(_day.text,
                style: _day.type == DayType.disabled
                    ? TextStyle(
                        color: Colors.black38, fontSize: style.dayFontSize)
                    : _textStyle),
            Positioned(
              child: Text(
                _day.bottomInfo ?? "",
                style: _day.type == DayType.disabled
                    ? TextStyle(color: Colors.black38)
                    : myDayStyle.textStyle,
              ),
              bottom: 6,
            )
          ],
        ),
      ),
    );
  }
}
