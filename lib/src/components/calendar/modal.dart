part of wy_calendar;

enum CalenderType { single, multiple, range }

enum DayType { selected, start, middle, end, disabled }

class Date {
  Date({this.date, this.type, this.text, this.topInfo, this.bottomInfo});

  DateTime date;
  DayType type;
  String text;
  String topInfo;
  String bottomInfo;

  void setMonth({int num = 1}) {
    int day = 0;
    int year = date.year;
    int month = date.month;
    for (var i = 0; i < num; i++) {
      month = month + 1 > 12 ? 1 : month + 1;
      year = month + 1 > 12 ? year + 1 : year;
      day = day + getMonthCount(year: year, month: month).length;
    }
    date = date.add(Duration(days: day));
  }

  /// 判断平年闰年；
  bool isLeapYear({int year}) {
    year = year ?? date.year;
    return (year % 400 == 0) || (year % 100 != 0 && year % 4 == 0);
  }

  /// 获取每个月的天数
  List<int> getMonthCount({int year, int month}) {
    year = year ?? date.year;
    month = month ?? date.month;

    final List monthDays = [31, null, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    int count = monthDays[month - 1] ?? (isLeapYear(year: year) ? 29 : 28);

    return List.generate(count, (day) => day + 1);
  }

  /// 比较两个时间的大小
  bool operator >=(Date myDate) {
    return date.isAfter(myDate.date) || date == myDate.date;
  }
}

class DayStyle {
  DayStyle(
      {this.time,
      this.selectedDateList,
      this.type,
      this.color = Colors.white}) {
    init();
  }

  final DateTime time;
  final List<DateTime> selectedDateList;
  final CalenderType type;
  final Color color;

  double opacity = 0.0;
  String text = '';
  BoxDecoration boxDecoration;
  TextStyle textStyle = TextStyle();
  DayType dayType;

  void init() {
    switch (type) {
      case CalenderType.single:
        if (selectedDateList.indexOf(time) > -1) {
          boxDecoration = BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(Radius.circular(4.0)));
          textStyle = TextStyle(color: Colors.white);
          dayType = DayType.selected;
        }
        break;
      case CalenderType.multiple:
        if (selectedDateList.indexOf(time) > -1) {
          DateTime beforeDay = time.add(Duration(days: -1));
          DateTime afterDay = time.add(Duration(days: 1));
          BorderRadius radius;
          if (selectedDateList.indexOf(beforeDay) > -1 &&
              selectedDateList.indexOf(afterDay) > -1) {
            radius = null;
          } else if (selectedDateList.indexOf(beforeDay) > -1 &&
              selectedDateList.indexOf(afterDay) == -1) {
            radius = BorderRadius.only(
                bottomRight: Radius.circular(4.0),
                topRight: Radius.circular(4.0));
          } else if (selectedDateList.indexOf(beforeDay) == -1 &&
              selectedDateList.indexOf(afterDay) > -1) {
            radius = BorderRadius.only(
                bottomLeft: Radius.circular(4.0),
                topLeft: Radius.circular(4.0));
          } else {
            radius = BorderRadius.all(Radius.circular(4.0));
          }
          boxDecoration = BoxDecoration(color: color, borderRadius: radius);
          textStyle = TextStyle(color: Colors.white);
          dayType = DayType.selected;
        }
        break;
      case CalenderType.range:
        if (selectedDateList.length == 1) {
          if (selectedDateList.indexOf(time) > -1) {
            boxDecoration = BoxDecoration(
                color: color,
                borderRadius: BorderRadius.all(Radius.circular(4.0)));
            textStyle = TextStyle(color: Colors.white);
            opacity = 1.0;
            text = "开始";
            dayType = DayType.start;
          }
        } else if (selectedDateList.length == 2) {
          if (time.isAfter(selectedDateList[0]) &&
              time.isBefore(selectedDateList[1])) {
            boxDecoration = BoxDecoration(
              color: color.withOpacity(0.1),
            );
            textStyle = TextStyle(color: color);
            dayType = DayType.middle;
          } else if (time == selectedDateList[0]) {
            boxDecoration = BoxDecoration(
                color: color,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(4.0),
                    topLeft: Radius.circular(4.0)));
            textStyle = TextStyle(color: Colors.white);
            opacity = 1.0;
            text = "开始";
            dayType = DayType.start;
          } else if (time == selectedDateList[1]) {
            boxDecoration = BoxDecoration(
                color: color,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(4.0),
                    topRight: Radius.circular(4.0)));
            textStyle = TextStyle(color: Colors.white);
            opacity = 1.0;
            text = "结束";
            dayType = DayType.end;
          }
        }
        break;
    }
  }
}

class WyCalenderController extends ScrollController {
  WyCalenderController({double initialScrollOffset = 0.0})
      : super(initialScrollOffset: initialScrollOffset);
  List<DateTime> times;

  DateTime toDate;

  void reset(List<DateTime> _times) {
    times = _times;
    notifyListeners();
    times = null;
  }

  void scrollToDate(DateTime date) {
    toDate = date;
    notifyListeners();
    toDate = null;
  }
}

// class WyCalenderController extends ChangeNotifier {
//   List<DateTime> times;

//   void reset(List<DateTime> _times) {
//     times = _times;
//     notifyListeners();
//     times = null;
//   }
// }
