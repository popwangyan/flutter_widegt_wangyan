part of wy_toast;

class WyToastStyle with Diagnosticable {
  WyToastStyle(
      {this.toastMaxWidth,
      this.toastFontSize,
      this.toastTextColor,
      this.toastLoadingIconColor,
      this.toastLineHeight,
      this.toastBorderRadius,
      this.toastBackgroundColor,
      this.toastOverlayBackgroundColor,
      this.toastIconSize,
      this.toastTextPadding,
      this.toastDefaultPadding,
      this.toastDefaultWidth,
      this.toastPositionTopDistance,
      this.toastPositionBottomDistance});

  const WyToastStyle.row({
    @required this.toastMaxWidth,
    @required this.toastFontSize,
    @required this.toastTextColor,
    @required this.toastLoadingIconColor,
    @required this.toastLineHeight,
    @required this.toastBorderRadius,
    @required this.toastBackgroundColor,
    @required this.toastOverlayBackgroundColor,
    @required this.toastIconSize,
    @required this.toastTextPadding,
    @required this.toastDefaultPadding,
    @required this.toastDefaultWidth,
    @required this.toastPositionTopDistance,
    @required this.toastPositionBottomDistance,
  })  : assert(toastMaxWidth != null),
        assert(toastFontSize != null),
        assert(toastTextColor != null),
        assert(toastLoadingIconColor != null),
        assert(toastLineHeight != null),
        assert(toastBorderRadius != null),
        assert(toastBackgroundColor != null),
        assert(toastIconSize != null),
        assert(toastTextPadding != null),
        assert(toastDefaultPadding != null),
        assert(toastDefaultWidth != null),
        assert(toastPositionTopDistance != null),
        assert(toastOverlayBackgroundColor != null),
        assert(toastPositionBottomDistance != null);

  final String toastMaxWidth;
  final double toastFontSize;
  final Color toastTextColor;
  final Color toastLoadingIconColor;
  final double toastLineHeight;
  final Radius toastBorderRadius;
  final Color toastBackgroundColor;
  final Color toastOverlayBackgroundColor;
  final double toastIconSize;
  final EdgeInsets toastTextPadding;
  final double toastDefaultPadding;
  final double toastDefaultWidth;
  final String toastPositionTopDistance;
  final String toastPositionBottomDistance;

  WyToastStyle copyWith({
    String toastMaxWidth,
    double toastFontSize,
    Color toastTextColor,
    Color toastLoadingIconColor,
    double toastLineHeight,
    Radius toastBorderRadius,
    Color toastBackgroundColor,
    double toastIconSize,
    EdgeInsets toastTextPadding,
    double toastDefaultPadding,
    double toastDefaultWidth,
    String toastPositionTopDistance,
    String toastPositionBottomDistance,
  }) {
    return WyToastStyle.row(
        toastMaxWidth: toastMaxWidth ?? this.toastMaxWidth,
        toastFontSize: toastFontSize ?? this.toastFontSize,
        toastTextColor: toastTextColor ?? this.toastTextColor,
        toastLoadingIconColor:
            toastLoadingIconColor ?? this.toastLoadingIconColor,
        toastLineHeight: toastLineHeight ?? this.toastLineHeight,
        toastBorderRadius: toastBorderRadius ?? this.toastBorderRadius,
        toastBackgroundColor: toastBackgroundColor ?? this.toastBackgroundColor,
        toastOverlayBackgroundColor:
            toastOverlayBackgroundColor ?? this.toastOverlayBackgroundColor,
        toastIconSize: toastIconSize ?? this.toastIconSize,
        toastTextPadding: toastTextPadding ?? this.toastTextPadding,
        toastDefaultPadding: toastDefaultPadding ?? this.toastDefaultPadding,
        toastDefaultWidth: toastDefaultWidth ?? this.toastDefaultWidth,
        toastPositionTopDistance:
            toastPositionTopDistance ?? this.toastPositionTopDistance,
        toastPositionBottomDistance:
            toastPositionBottomDistance ?? this.toastPositionBottomDistance);
  }

  WyToastStyle minxiWith(WyToastStyle style) {
    return WyToastStyle.row(
        toastMaxWidth: style.toastMaxWidth ?? this.toastMaxWidth,
        toastFontSize: style.toastFontSize ?? this.toastFontSize,
        toastTextColor: style.toastTextColor ?? this.toastTextColor,
        toastLoadingIconColor:
            style.toastLoadingIconColor ?? this.toastLoadingIconColor,
        toastLineHeight: style.toastLineHeight ?? this.toastLineHeight,
        toastBorderRadius: style.toastBorderRadius ?? this.toastBorderRadius,
        toastBackgroundColor:
            style.toastBackgroundColor ?? this.toastBackgroundColor,
        toastOverlayBackgroundColor: style.toastOverlayBackgroundColor ??
            this.toastOverlayBackgroundColor,
        toastIconSize: style.toastIconSize ?? this.toastIconSize,
        toastTextPadding: style.toastTextPadding ?? this.toastTextPadding,
        toastDefaultPadding:
            style.toastDefaultPadding ?? this.toastDefaultPadding,
        toastDefaultWidth: style.toastDefaultWidth ?? this.toastDefaultWidth,
        toastPositionTopDistance:
            style.toastPositionTopDistance ?? this.toastPositionTopDistance,
        toastPositionBottomDistance: style.toastPositionBottomDistance ??
            this.toastPositionBottomDistance);
  }

  static const WyToastStyle defaultStyle = const WyToastStyle.row(
      toastMaxWidth: "70%",
      toastFontSize: 16,
      toastTextColor: Colors.white,
      toastLoadingIconColor: Colors.white,
      toastLineHeight: 20,
      toastBorderRadius: Radius.circular(8.0),
      toastBackgroundColor: Color.fromRGBO(0, 0, 0, 0.7),
      toastOverlayBackgroundColor: Color.fromRGBO(0, 0, 0, 0.3),
      toastIconSize: 60,
      toastTextPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      toastDefaultPadding: 30,
      toastDefaultWidth: 150,
      toastPositionTopDistance: "20%",
      toastPositionBottomDistance: "20%");
}
