library wy_toast;

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_widegt_wangyan/src/utils.dart';
part 'box.dart';
part 'style.dart';

enum ToastType { loading, message, success, fail, widget }

enum Position { top, center, bottom }

abstract class WyShowToast {
  static OverlayEntry _entry;

  static Timer _timer;

  WyShowToast(this.context) {
    if (_entry != null) {
      _entry.remove();
      if (_timer != null) _timer.cancel();
    }
    bool flag = WyShowToast._entry != null;
    overlayState = Overlay.of(context);
    overlayEntry = OverlayEntry(builder: (context) => build(context, flag));
    overlayState.insert(overlayEntry);
    WyShowToast._entry = overlayEntry;

    if (_duration != 0) {
      WyShowToast._timer =
          Timer.periodic(Duration(milliseconds: _duration), (timer) {
        clear();
      });
    }
  }

  final BuildContext context;

  OverlayState overlayState;

  OverlayEntry overlayEntry;

  Widget build(BuildContext context, bool hasEntry);

  int get _duration => null;

  void clear() {
    if (overlayEntry != null) {
      overlayEntry.remove();
      overlayEntry = null;
      _entry = null;
    }
  }
}

class ToastBox extends StatelessWidget {
  ToastBox(
      {Key key,
      this.message,
      this.type,
      this.position,
      this.icon,
      this.forbidClick,
      this.overlay,
      this.style,
      this.iconBuilder,
      this.animationTime})
      : super(key: key);
  final String message;
  final ToastType type;
  final Position position;
  final IconData icon;
  final Duration animationTime;
  final bool forbidClick;
  final bool overlay;
  final WyToastStyle style;
  final WidgetBuilder iconBuilder;

  @override
  Widget build(BuildContext context) {
    return PositionWidget(
      forbidClick: forbidClick,
      position: position,
      overlay: overlay,
      style: style,
      child: BoxFadeTransition(
          duration: animationTime,
          child: () {
            if (type == ToastType.message && iconBuilder == null) {
              return messageBox(context);
            } else {
              return content(context);
            }
          }()),
    );
  }

  Widget content(BuildContext context) {
    return Container(
      width: style.toastDefaultWidth,
      height: style.toastDefaultWidth,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 72,
            height: 72,
            child: Center(
              child: () {
                if (iconBuilder != null) return iconBuilder(context);
                switch (type) {
                  case ToastType.loading:
                    return loading();
                    break;
                  case ToastType.success:
                    return iconWidget(Icons.done);
                    break;
                  case ToastType.fail:
                    return iconWidget(Icons.error);
                    break;
                  case ToastType.widget:
                    return iconWidget(icon);
                    break;
                  default:
                    return iconWidget(icon);
                }
              }(),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 0),
            child: Text(
              message,
              style: TextStyle(
                  height: style.toastLineHeight / style.toastFontSize,
                  fontSize: style.toastFontSize,
                  color: style.toastTextColor),
            ),
          )
        ],
      ),
      decoration: ShapeDecoration(
          color: style.toastBackgroundColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(style.toastBorderRadius))),
    );
  }

  Widget messageBox(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
          maxWidth: Utils.getNumber(context, style.toastMaxWidth)),
      child: Padding(
        padding: style.toastTextPadding,
        child: Text(
          message,
          style: TextStyle(
              height: style.toastLineHeight / style.toastFontSize,
              fontSize: style.toastFontSize,
              color: style.toastTextColor),
        ),
      ),
      decoration: ShapeDecoration(
          color: style.toastBackgroundColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(style.toastBorderRadius))),
    );
  }

  Widget loading() {
    return SizedBox(
      width: style.toastIconSize * 0.6,
      height: style.toastIconSize * 0.6,
      child: CircularProgressIndicator(
        strokeWidth: 3.0,
        valueColor:
            new AlwaysStoppedAnimation<Color>(style.toastLoadingIconColor),
      ),
    );
  }

  Widget iconWidget(IconData iconData) {
    return Icon(
      iconData,
      size: style.toastIconSize,
      color: Colors.white,
    );
  }
}

class WyToast extends WyShowToast {
  WyToast(BuildContext context,
      {@required this.message,
      this.forbidClick = false,
      this.duration = 2000,
      this.animationTime = const Duration(milliseconds: 200),
      this.position = Position.center,
      this.style,
      this.iconData,
      this.overlay = false,
      this.iconBuilder,
      this.type = ToastType.message})
      : super(context) {
    if (type == ToastType.widget) {
      assert(iconData != null);
    }
  }

  final String message;
  final bool forbidClick;
  final int duration;
  final Duration animationTime;
  final Position position;
  final IconData iconData;
  final ToastType type;
  final WyToastStyle style;
  final bool overlay;
  final WidgetBuilder iconBuilder;

  @override
  Widget build(BuildContext context, bool hasEntry) {
    WyToastStyle _style = style != null
        ? WyToastStyle.defaultStyle.minxiWith(style)
        : WyToastStyle.defaultStyle;
    return ToastBox(
      message: message,
      type: type,
      position: position,
      icon: iconData,
      style: _style,
      iconBuilder: iconBuilder,
      forbidClick: forbidClick,
      overlay: overlay,
      animationTime: hasEntry ? Duration(seconds: 0) : animationTime,
    );
  }

  @override
  int get _duration => duration;

  static WyToast fail(BuildContext context,
      {@required String message,
      bool forbidClick = false,
      bool overlay = false,
      Position position = Position.center,
      int duration = 2000,
      Duration animationTime = const Duration(milliseconds: 200),
      WyToastStyle style}) {
    return WyToast(context,
        message: message,
        forbidClick: forbidClick,
        overlay: overlay,
        position: position,
        duration: duration,
        animationTime: animationTime,
        style: style,
        type: ToastType.fail);
  }

  static WyToast success(BuildContext context,
      {@required String message,
      bool forbidClick = false,
      bool overlay = false,
      Position position = Position.center,
      int duration = 2000,
      Duration animationTime = const Duration(milliseconds: 200),
      WyToastStyle style}) {
    return WyToast(context,
        message: message,
        forbidClick: forbidClick,
        overlay: overlay,
        position: position,
        duration: duration,
        animationTime: animationTime,
        style: style,
        type: ToastType.success);
  }

  static WyToast loading(BuildContext context,
      {@required String message,
      bool forbidClick = true,
      bool overlay = false,
      Position position = Position.center,
      int duration = 2000,
      Duration animationTime = const Duration(milliseconds: 200),
      WyToastStyle style}) {
    return WyToast(context,
        message: message,
        forbidClick: forbidClick,
        overlay: overlay,
        position: position,
        duration: duration,
        animationTime: animationTime,
        style: style,
        type: ToastType.loading);
  }

  static void remove() {
    if (WyShowToast._entry != null) {
      WyShowToast._entry.remove();
      WyShowToast._entry = null;
    }
  }
}

// 动画
class BoxFadeTransition extends StatefulWidget {
  BoxFadeTransition({
    Key key,
    @required this.duration,
    @required this.child,
  }) : super(key: key);

  final Widget child;
  final Duration duration;

  @override
  _BoxFadeTransitionState createState() => _BoxFadeTransitionState();
}

class _BoxFadeTransitionState extends State<BoxFadeTransition>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      child: widget.child,
      opacity: _animation,
    );
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
    _animation = Tween(begin: 0.0, end: 1.0).animate(_animationController);
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
