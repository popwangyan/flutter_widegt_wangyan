part of wy_toast;

// ignore: must_be_immutable
class PositionWidget extends StatelessWidget {
  PositionWidget(
      {Key key,
      this.child,
      this.position,
      this.forbidClick,
      this.overlay,
      this.style})
      : super(key: key);

  final Position position;
  final Widget child;
  final bool forbidClick;
  final bool overlay;
  final WyToastStyle style;

  BuildContext _context;

  Matrix4 get _position {
    Matrix4 _matrix4;
    switch (position) {
      case Position.top:
        double _distance =
            Utils.getNumber(_context, style.toastPositionTopDistance) -
                Utils.getNumber(_context, "50%");
        _matrix4 = Matrix4.translationValues(0, _distance, 0);
        break;
      case Position.bottom:
        double _distance = Utils.getNumber(_context, "50%") -
            Utils.getNumber(_context, style.toastPositionBottomDistance);
        _matrix4 = Matrix4.translationValues(0, _distance, 0);
        break;
      default:
        _matrix4 = Matrix4.translationValues(0, 0, 0);
    }
    return _matrix4;
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    if (forbidClick || overlay) {
      return _forbidClickBox(child, context);
    }
    return _ableClickBox(child);
  }

  Widget _forbidClickBox(Widget child, BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: overlay ? style.toastOverlayBackgroundColor : Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            transform: _position,
            child: Material(
              type: MaterialType.transparency,
              child: child,
            ),
          )
        ],
      ),
    );
  }

  Widget _ableClickBox(Widget child) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          transform: _position,
          child: Material(
            type: MaterialType.transparency,
            child: child,
          ),
        )
      ],
    );
  }
}
