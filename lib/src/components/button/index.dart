library wy_buttom;

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_widegt_wangyan/src/utils.dart';

part 'style.dart';

enum ButtonType { primary, info, warning, danger }

enum LoadingType { spinner, circular }

enum ShapeType { square, round }

enum ButtonSize { large, normal, small, mini }

// ignore: must_be_immutable
class WyButton extends StatelessWidget {
  WyButton(
      {Key key,
      this.text = '按钮',
      this.type = ButtonType.primary,
      this.plain = false,
      this.hairline = false,
      this.loading = false,
      this.block = false,
      this.shape = ShapeType.square,
      this.loadingType = LoadingType.circular,
      this.size = ButtonSize.normal,
      this.disabled = false,
      this.icon,
      this.color,
      this.style,
      this.onClick})
      : super(key: key);
  final String text;
  final ButtonType type;
  final bool plain;
  final bool hairline;
  final bool loading;
  final bool disabled;
  final bool block;
  final LoadingType loadingType;
  final ShapeType shape;
  final IconData icon;
  final ButtonSize size;
  final Color color;
  final VoidCallback onClick;
  final WyButtonStyle style;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data:
          Theme.of(context).copyWith(buttonTheme: ButtonThemeData(minWidth: 0)),
      child: Container(
        child: FlatButton(
          padding: _edgeInsetsGeometry,
          highlightColor: Utils.setRGB(_buttonStyle.backgroundColor),
          splashColor: Colors.transparent,
          disabledColor: _buttonStyle.backgroundColor.withOpacity(0.5),
          disabledTextColor: _buttonStyle.textColor,
          color: _buttonStyle.backgroundColor,
          textColor: _buttonStyle.textColor,
          shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: _buttonStyle.borderColor, width: hairline ? 1.0 : 1.5),
              borderRadius: BorderRadius.all(
                  Radius.circular(shape == ShapeType.square ? 4.0 : 26.0))),
          onPressed: disabled ? null : onClick,
          child: Container(
            child: Row(
              mainAxisSize: _mainAxisSize,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                loadingBox(),
                iconWidget(),
                Text(
                  text,
                  style: TextStyle(fontSize: 16.0),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// 根据参数获取主题样式
  WyButtonStyle get _buttonStyle {
    WyButtonStyle _buttonStyle = WyButtonStyle.defaultTheme;
    Color primaryColor = Colors.white;
    switch (type) {
      case ButtonType.primary:
        primaryColor = ColorHex('#07c160');
        break;
      case ButtonType.danger:
        primaryColor = ColorHex('#ee0a24');
        break;
      case ButtonType.info:
        primaryColor = ColorHex('#1989fa');
        break;
      case ButtonType.warning:
        primaryColor = ColorHex('#ff976a');
        break;
    }
    if (color != null) {
      primaryColor = color;
      _buttonStyle = _buttonStyle.copyWith(
          backgroundColor: primaryColor,
          textColor: Colors.white,
          borderColor: Colors.transparent);
    }
    if (type != null) {
      if (plain) {
        _buttonStyle = _buttonStyle.copyWith(
            backgroundColor: Colors.white,
            textColor: primaryColor,
            borderColor: primaryColor);
      } else {
        _buttonStyle = _buttonStyle.copyWith(
            backgroundColor: primaryColor,
            textColor: Colors.white,
            borderColor: Colors.transparent);
      }
    }
    return style != null ? _buttonStyle.minxiWith(style) : _buttonStyle;
  }

  /// 根据size参数获取按钮的padding值
  EdgeInsets get _edgeInsetsGeometry {
    EdgeInsets result;
    switch (size) {
      case ButtonSize.large:
        result = _buttonStyle.largePadding;
        break;
      case ButtonSize.normal:
        result = _buttonStyle.normalPadding;
        break;
      case ButtonSize.small:
        result = _buttonStyle.smallPadding;
        break;
      case ButtonSize.mini:
        result = _buttonStyle.miniPadding;
        break;
    }
    return result;
  }

  ///
  MainAxisSize get _mainAxisSize {
    if (size == ButtonSize.large || block) {
      return MainAxisSize.max;
    }
    return MainAxisSize.min;
  }

  Widget loadingBox() {
    if (!loading) {
      return Container(
        width: 0,
        height: 0,
      );
    }

    Widget loadingWidget;
    if (loadingType == LoadingType.circular) {
      loadingWidget = CircularProgressIndicator(
        strokeWidth: 2,
        valueColor: new AlwaysStoppedAnimation<Color>(_buttonStyle.textColor),
      );
    } else {
      loadingWidget = CupertinoActivityIndicator();
    }
    return Container(
      width: 24.0,
      height: 24.0,
      padding: EdgeInsets.all(3.0),
      margin: EdgeInsets.only(right: 6.0),
      child: loadingWidget,
    );
  }

  Widget iconWidget() {
    if (icon == null) {
      return Container(
        width: 0,
        height: 0,
      );
    }
    return Container(
      child: Icon(
        icon,
        size: 20,
        color: plain ? _buttonStyle.backgroundColor : _buttonStyle.textColor,
      ),
      margin: EdgeInsets.only(right: 6.0),
    );
  }
}
