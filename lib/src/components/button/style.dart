part of wy_buttom;

class WyButtonStyle {
  const WyButtonStyle(
      {this.backgroundColor,
      this.borderColor,
      this.textColor,
      this.miniPadding,
      this.smallPadding,
      this.normalPadding,
      this.largePadding});

  const WyButtonStyle.row({
    @required this.backgroundColor,
    @required this.borderColor,
    @required this.textColor,
    @required this.miniPadding,
    @required this.smallPadding,
    @required this.normalPadding,
    @required this.largePadding,
  })  : assert(backgroundColor != null),
        assert(backgroundColor != null),
        assert(backgroundColor != null),
        assert(miniPadding != null),
        assert(smallPadding != null),
        assert(normalPadding != null),
        assert(largePadding != null);

  final Color backgroundColor;
  final Color borderColor;
  final Color textColor;
  final EdgeInsets miniPadding;
  final EdgeInsets smallPadding;
  final EdgeInsets normalPadding;
  final EdgeInsets largePadding;

  WyButtonStyle copyWith({
    Color backgroundColor,
    Color borderColor,
    Color textColor,
    EdgeInsets miniPadding,
    EdgeInsets smallPadding,
    EdgeInsets normalPadding,
    EdgeInsets largePadding,
  }) {
    return WyButtonStyle.row(
        backgroundColor: backgroundColor ?? this.backgroundColor,
        borderColor: borderColor ?? this.borderColor,
        textColor: textColor ?? this.textColor,
        miniPadding: miniPadding ?? this.miniPadding,
        smallPadding: smallPadding ?? this.smallPadding,
        normalPadding: normalPadding ?? this.normalPadding,
        largePadding: largePadding ?? this.largePadding);
  }

  WyButtonStyle minxiWith(WyButtonStyle theme) {
    return WyButtonStyle.row(
        backgroundColor: theme.backgroundColor ?? this.backgroundColor,
        borderColor: theme.borderColor ?? this.borderColor,
        textColor: theme.textColor ?? this.textColor,
        miniPadding: theme.miniPadding ?? this.miniPadding,
        smallPadding: theme.smallPadding ?? this.smallPadding,
        normalPadding: theme.normalPadding ?? this.normalPadding,
        largePadding: theme.largePadding ?? this.largePadding);
  }

  static const WyButtonStyle defaultTheme = const WyButtonStyle.row(
      backgroundColor: Colors.white,
      borderColor: Color(0xffeaebec),
      textColor: Colors.black,
      miniPadding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0),
      smallPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      normalPadding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 12.0),
      largePadding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0));
}
