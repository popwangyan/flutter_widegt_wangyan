part of wy_cascader;

class CascaderTitle extends StatelessWidget {
  const CascaderTitle({Key key, this.title = "请选择所在地区", this.close})
      : super(key: key);

  final String title;
  final Function close;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: Utils.getNumber(context, "60%",
                direction: PDirection.horizontal),
            child: Text(
              title,
              style: TextStyle(fontSize: 16),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          GestureDetector(
            child: Icon(
              Icons.clear,
              color: Colors.black38,
            ),
            onPanDown: (e) {
              close();
            },
          )
        ],
      ),
    );
  }
}

class CascaderTabsBar extends StatefulWidget {
  const CascaderTabsBar(
      {Key key,
      this.placeholder,
      this.activeColor,
      this.value,
      @required this.controller})
      : assert(controller != null),
        super(key: key);

  final List<WyCascaderData> value;
  final TabController controller;
  final String placeholder;
  final Color activeColor;

  @override
  _CascaderTabsBarState createState() => _CascaderTabsBarState();
}

class _CascaderTabsBarState extends State<CascaderTabsBar> {
  double underlineWidth = 40;
  double titleBoxWidth = 60;

  double get currentOffset {
    return widget.controller.animation.value * titleBoxWidth +
        (titleBoxWidth - underlineWidth) / 2;
  }

  List<WyCascaderData> get value {
    return widget.value ?? [WyCascaderData()];
  }

  int titleIndex = 0;

  _onTap(int index) {
    widget.controller.animateTo(index);
  }

  _listener() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    widget.controller.animation.addListener(_listener);
  }

  @override
  void dispose() {
    super.dispose();
    widget.controller.removeListener(_listener);
  }

  @override
  void didUpdateWidget(CascaderTabsBar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_listener);
    }
    widget.controller.animation.addListener(_listener);
    _onTap(value.length - 1);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 63,
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
      child: Column(
        children: <Widget>[
          Container(
            height: 60,
            child: Row(
              children: value
                  .map((item) => itemBox(item.text, value.indexOf(item)))
                  .toList(),
            ),
          ),
          underLine()
        ],
      ),
    );
  }

  Widget underLine() {
    return Stack(
      children: <Widget>[
        Container(
          height: 3,
        ),
        Positioned(
            top: 0,
            left: currentOffset,
            child: Container(
              height: 3,
              width: underlineWidth,
              color: widget.activeColor,
            ))
      ],
    );
  }

  Widget itemBox(String title, int index) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        _onTap(index);
      },
      child: Container(
        alignment: Alignment.center,
        constraints: BoxConstraints(maxWidth: 60, minWidth: 60),
        padding: EdgeInsets.only(top: 10, bottom: 10),
        child: title == null
            ? Text(
                widget.placeholder,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.black38),
              )
            : Text(
                title,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
      ),
    );
  }
}

class CascaderTabsbarViews extends StatefulWidget {
  const CascaderTabsbarViews(
      {Key key,
      this.data,
      this.activeColor,
      this.value,
      this.controller,
      this.callback})
      : assert(controller != null),
        assert(callback != null),
        super(key: key);

  final List<WyCascaderData> data;
  final List<WyCascaderData> value;
  final TabController controller;
  final CascaderCallBack callback;
  final Color activeColor;

  @override
  _CascaderTabsbarViewsState createState() => _CascaderTabsbarViewsState();
}

class _CascaderTabsbarViewsState extends State<CascaderTabsbarViews> {
  TabController _controller;

  List<List<WyCascaderData>> _list = [];

  void initData() {
    _list.clear();
    List<WyCascaderData> _data = widget.data;
    for (var i = 0; i < widget.value.length; i++) {
      _list.add(_data);
      if (widget.value[i].value == null) break;
      _data =
          _data.firstWhere((e) => widget.value[i].value == e.value).children;
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = widget.controller;
    initData();
  }

  @override
  void didUpdateWidget(CascaderTabsbarViews oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      _controller = widget.controller;
    }
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 384,
      child: TabBarView(
        children:
            _list.map((item) => viewPort(item, _list.indexOf(item))).toList(),
        controller: _controller,
      ),
    );
  }

  Widget viewPort(List<WyCascaderData> list, int index) {
    if (list.isEmpty)
      return Container(
        child: Center(
          child: SizedBox(
            width: 26,
            height: 26,
            child: CircularProgressIndicator(
              strokeWidth: 3,
              valueColor: AlwaysStoppedAnimation<Color>(widget.activeColor),
            ),
          ),
        ),
      );
    return ListView.builder(
      itemBuilder: (BuildContext context, int itemIndex) => Container(
        height: 50,
        child: viewPortItem(list[itemIndex], index,
            checked: list[itemIndex].value == widget.value[index].value),
      ),
      itemCount: list.length,
      itemExtent: 50,
      padding: EdgeInsets.all(0),
    );
  }

  Widget viewPortItem(WyCascaderData item, int index, {bool checked = false}) {
    return FlatButton(
        onPressed: () {
          WyCascaderData _data =
              WyCascaderData(text: item.text, value: item.value);
          List<WyCascaderData> value = widget.value;
          if (value.last.value != null) value.add(WyCascaderData());
          if (value.length > (index + 1))
            value.removeRange(index, value.length - 1);
          value.insert((value.length - 1), _data);
          if (item.children == null) value.removeLast();

          if (widget.callback != null) {
            widget.callback(value, item.children == null);
          }
        },
        hoverColor: Color.fromRGBO(242, 243, 245, 1),
        highlightColor: Color.fromRGBO(242, 243, 245, 1),
        splashColor: Colors.transparent,
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              item.text,
              style: checked
                  ? TextStyle(fontSize: 14, color: widget.activeColor)
                  : TextStyle(fontSize: 14, color: Colors.black87),
            ),
            Visibility(
              child: Icon(
                Icons.check,
                color: widget.activeColor,
              ),
              visible: checked,
            )
          ],
        ));
  }
}
