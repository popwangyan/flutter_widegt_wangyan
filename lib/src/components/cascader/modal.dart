part of wy_cascader;

class WyCascaderData<T> {
  WyCascaderData({this.text, this.value, this.children, this.checked = false});

  String text;
  T value;
  bool checked;
  List<WyCascaderData<T>> children;
}
