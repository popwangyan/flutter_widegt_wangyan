library wy_cascader;

import 'package:flutter/material.dart';
import '../popup/index.dart';
import '../../utils.dart';

part 'modal.dart';

part 'widgets.dart';

typedef CascaderCallBack = void Function(
    List<WyCascaderData> value, bool isLast);

typedef CascaderEvent = void Function(dynamic value,
    List<WyCascaderData> selectedOptions, int tabIndex, Function setState);

class WyCascader {
  static Future<T> show<T>(
    BuildContext context, {
    String title = "请选择所在地区",
    List<WyCascaderData> value,
    List<WyCascaderData> options,
    String placeholder = "请选择",
    Color activeColor = Colors.red,
    bool closeable = true,
    CascaderEvent fisinsh,
    CascaderEvent change,
    Function close,
  }) {
    return WyPopup.showSheet<T>(context,
        height: 384.0 + 63 + 48,
        round: true,
        close: () {
          Navigator.of(context).pop();
        },
        closeOnClickOverlay: false,
        builder: (context) => CascaderBox(
            title: title,
            value: value,
            options: options,
            placeholder: placeholder,
            activeColor: activeColor,
            fisinsh: fisinsh,
            change: change,
            close: close));
  }
}

class CascaderBox extends StatefulWidget {
  const CascaderBox(
      {Key key,
      this.title,
      this.value,
      this.options,
      this.placeholder,
      this.activeColor,
      this.fisinsh,
      this.change,
      this.close})
      : super(key: key);

  final String title;
  final List<WyCascaderData> value;
  final List<WyCascaderData> options;
  final String placeholder;
  final Color activeColor;
  final CascaderEvent fisinsh;
  final CascaderEvent change;
  final Function close;

  @override
  _CascaderBoxState createState() => _CascaderBoxState();
}

class _CascaderBoxState extends State<CascaderBox>
    with TickerProviderStateMixin {
  TabController _tabController;

  List<WyCascaderData> options;

  void setTabController({bool isInit = false}) {
    _tabController = TabController(length: value.length, vsync: this);
    _tabController.index = value.length - (isInit ? 1 : 2);
  }

  @override
  void initState() {
    super.initState();
    value = widget.value ?? [WyCascaderData()];
    options = widget.options;
    setTabController(isInit: true);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          CascaderTitle(
            title: widget.title,
            close: () {
              if (widget.close != null) widget.close();
              Navigator.pop(context);
            },
          ),
          CascaderTabsBar(
            value: value,
            controller: _tabController,
            placeholder: widget.placeholder,
            activeColor: widget.activeColor,
          ),
          CascaderTabsbarViews(
              controller: _tabController,
              value: value,
              data: options,
              activeColor: widget.activeColor,
              callback: (List<WyCascaderData> currentValue, bool islast) {
                value = currentValue;
                WyCascaderData _value;
                List<WyCascaderData> _selectedOptions;
                int _tabIndex;
                if (!islast) {
                  _value = value[value.length - 2];
                  _selectedOptions =
                      value.getRange(0, value.length - 1).toList();
                  _tabIndex = _selectedOptions.length - 1;
                  setTabController();
                } else {
                  _value = value[value.length - 1];
                  _selectedOptions = value;
                  _tabIndex = _selectedOptions.length - 1;
                  if (widget.fisinsh != null) {
                    widget.fisinsh(
                        _value.value, _selectedOptions, _tabIndex, () {});
                  }
                  Navigator.pop(context);
                }
                if (widget.change != null)
                  widget.change(_value.value, _selectedOptions, _tabIndex, () {
                    if (this.mounted) setState(() {});
                  });
                setState(() {});
              }),
        ],
      ),
    );
  }

  List<WyCascaderData> value;
}
